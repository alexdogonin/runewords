﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GenericCollections = System.Collections.Generic;

namespace Diablo2ZyElRunewords.Entities
{
    public class IncorrectDataLineFormatException : ApplicationException
    {
        public IncorrectDataLineFormatException() : base(infoMessage) { }
        public IncorrectDataLineFormatException(DataLine dataLine) : base(infoMessage + string.Format(" \"{0}\"", dataLine.Data)) { }
        public IncorrectDataLineFormatException(DataLine dataLine, string expectedFormat):
             base(infoMessage + string.Format(" \"{0}\", ожидалось \"{1}\"", dataLine.Data, expectedFormat)) { }

        private static readonly string infoMessage = "Некорректный формат строки файла данных";
    }

    class RuneBindedToDataLine: Rune, IDisposable
    {
        #region public methods
        public RuneBindedToDataLine(DataLine dataLine)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(dataLine.Data, runeRegExp))
                throw new IncorrectDataLineFormatException(dataLine, "название_руны\tуровень");
            this.dataLine = dataLine;
            var parms = dataLine.Data.Split(parmsSeparator);
            Name = parms[0];
            Level = int.Parse(parms[1]);
            Comment = dataLine.Comment;
        }

        void IDisposable.Dispose()
        {
            dataLine.Data = string.Join(parmsSeparator.ToString(), Name, Level);
            dataLine.Comment = Comment;
        }
        #endregion

        #region private members
        private const char parmsSeparator = '\t';
        private const string runeRegExp = @"^\w+\t\d+$";

        private readonly DataLine dataLine;
        #endregion
    }

    class ItemClassBindedToDataLine
    {

    }

    class PersonClassBindedToDataLine
    {

    }

    class RunewordBindedToDataLine
    {

    }

    #region collections
    public class ItemClassesCatalog : IEnumerable<ItemClass>
    {
        // public methods
        public ItemClassesCatalog()
        {
            itemClasses = new GenericCollections.Dictionary<string, ItemClass>(50);
        }

        public ItemClassesCatalog(IEnumerable<ItemClass> itemClasses)
        {
            this.itemClasses = 
                new GenericCollections.Dictionary<string, ItemClass>(itemClasses.ToDictionary(itemClass => itemClass.ShortName));
        }

        public ItemClassesCatalog(ItemClassesCatalog itemClasses): this(itemClasses as IEnumerable<ItemClass>){        }

        public void Insert(ItemClass itemClass)
        {
            itemClasses.Add(itemClass.ShortName, itemClass);
        }

        public void Insert(string shortName, string fullName)
        {
            itemClasses.Add(shortName, new ItemClass(shortName, fullName));
        }

        public void Clear()
        {
            itemClasses.Clear();
        }

        // public properties
        public ItemClass this[string shortName]
        {
            get {
                ItemClass itemClass;
                itemClasses.TryGetValue(shortName, out itemClass);
                return itemClass;
            }
        }

        public int Count
        {
            get{return itemClasses.Count;}
        }

        // interface implementation
        IEnumerator<ItemClass> IEnumerable<ItemClass>.GetEnumerator()
        {
            return itemClasses.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return itemClasses.Values.GetEnumerator();
        }

        // private members
        private GenericCollections.Dictionary<string, ItemClass> itemClasses;
    }

    public class PersonClassesCatalog : IEnumerable<PersonClass>
    {
        // public methods
        public PersonClassesCatalog()
        {
            personClasses = new Dictionary<string, PersonClass>(9);
        }

        public PersonClassesCatalog(IEnumerable<PersonClass> personClasses)
        {
            this.personClasses = 
                new GenericCollections.Dictionary<string, PersonClass>(personClasses.ToDictionary(personClass => personClass.ShortName));
        }

        public void Insert(PersonClass personClass)
        {
            personClasses.Add(personClass.ShortName, personClass);
        }

        public void Insert(string shortName, string fullName)
        {
            personClasses.Add(shortName, new PersonClass(shortName, fullName));
        }

        public void Clear()
        {
            personClasses.Clear();
        }

        // public properties
        public PersonClass this[string shortName]
        {
            get {
                PersonClass personClass;
                personClasses.TryGetValue(shortName, out personClass);
                return personClass;
            }
        }

        public int Count
        {
            get{return personClasses.Count;}
        }

        // interface implementation
        IEnumerator<PersonClass> IEnumerable<PersonClass>.GetEnumerator()
        {
            return personClasses.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return personClasses.Values.GetEnumerator();
        }

        // private members
        private GenericCollections.Dictionary<string, PersonClass> personClasses;
    }

    
    #endregion

    public interface IEffect
    {}

    public class RunewordsCollection : IEnumerable<Runeword>
    {
        // public methods
        public RunewordsCollection()
        {
            runewords = new List<Runeword>();
        }

        public RunewordsCollection(IEnumerable<Runeword> collection)
        {
            runewords = new List<Runeword>(collection);
        }

        public void Insert(Runeword runeword)
        {
            runewords.Add(runeword);
        }

        public void Insert(string runeWordName, IEnumerable<ItemClass> itemClasses, int neededLevel,
            IEnumerable<PersonClass> personClasses, IEnumerable<Rune> runes, string comment = "")
        {
            runewords.Add(new Runeword(runeWordName, itemClasses, neededLevel, personClasses, runes, comment));
        }

        public void Clear()
        {
            runewords.Clear();
        }

        // Inteface implementation
        IEnumerator<Runeword> IEnumerable<Runeword>.GetEnumerator()
        {
            return runewords.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return runewords.GetEnumerator();
        }

        public int Count
        {
            get{return runewords.Count;}
        }

        // private members
        private GenericCollections.List<Runeword> runewords;
    }
}