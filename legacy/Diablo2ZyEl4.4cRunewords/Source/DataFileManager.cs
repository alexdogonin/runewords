﻿/*
    Редактор файла данных.
 * 
 * Файл данных условно разделён на блоки. 
 * 
 * Список блоков:
 * [Runes] - список рун и их уровней.
 *    Rune | Level
 * [ItemClasses]
 *    Symbol | FullName
 * [PersonsClasses]
 *    Symbol | FullName
 * [Runewords]
 *    Word | ItemClasses | Level | Class | Runeword | Comment
 *  Example:
 *    Трубкозуб | pe,ct | 120 | Д | Шаэль-Лем
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GenericCollections = System.Collections.Generic;

namespace Diablo2ZyElRunewords
{
    #region public clases
    // Exceptions
    public class DataFileNotReaded_Exception : ApplicationException
    {
        public DataFileNotReaded_Exception() : this(null) { }
        public DataFileNotReaded_Exception(Exception innerException) : base("Файл с данными не прочитан.", innerException) { }
    }

    public class DataFileNotExported_Exception : ApplicationException
    {
        public DataFileNotExported_Exception(Exception innerException) : base("Файл с данными не сохранён.", innerException) { }
        public DataFileNotExported_Exception() : this(null) { }
    }

    public class DataProcessorIsNotSet : ApplicationException
    {
        public DataProcessorIsNotSet(Exception innerException) : base("Не задан обработчик содержимого файла данных", innerException) { }
        public DataProcessorIsNotSet() : this(null) { }
    }

    public class DataFileInsertElem_NotFoundElemInsertAfter : ApplicationException
    {
        public DataFileInsertElem_NotFoundElemInsertAfter(Exception innerException) : base("Не найден элемент после которого необходимо выполнить вставку", innerException) { }
        public DataFileInsertElem_NotFoundElemInsertAfter() : this(null) { }
    }

    public class DataLine
    {
        public DataLine(string line) { Data = line; }
        public virtual string Data { get; set; }
    }
    // line as DataFileLine ?? line;

    public class DataLinesCollection: ICollection, ICollection<DataLine>
    {
        public bool Modified { get; private set; }

        int ICollection.Count { get { return lines.Count; } }
        object ICollection.SyncRoot { get { return lines} }
        bool ICollection.IsSynchronized { get; }

        void ICollection.CopyTo(Array array, int index) { }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return lines.GetEnumerator();
        }

        private List<DataLine> lines;
    }

    public class DataFileManager
    {
        #region public properties
        char CommentSymb { get { return '#'; } }
        #endregion

        #region public methodes
        public DataFileManager(string DataFileName)
        {
            dataFileInfo = new System.IO.FileInfo(DataFileName);
        }

        public void InsertAfter(DataLine line, DataLine newLine)
        {
            try
            {
                index = lines.Find();
                lines.Insert(index + 1, newLine);
            }
            catch(ApplicationException e)
            {
                throw new DataFileInsertElem_NotFoundElemInsertAfter(e);
            }
        }

        public IEnumerable<DataLine> GetDataLines()
        {
            if (null == lines) lines = readData();
            return lines.AsReadOnly().AsEnumerable();
        }
        #endregion

        #region private methodes
        private IEnumerable<DataFileLine> readData()
        {
            System.IO.TextReader reader = dataFileInfo.OpenText();
            return reader.ReadToEnd().Split('\n').Select(elem => new DataFileLine(elem));
        }

        private void writeData(IEnumerable<DataFileLine> lineObjects)
        {
            var writer = dataFileInfo.CreateText();
            writer.Write(lineObjects.Select(elem => elem.ToString() + "\n").ToString());
        }
        #endregion

        #region private members
        private System.IO.FileInfo dataFileInfo;
        #endregion
    }

    #endregion

    #region private classes
    class DataFileLine: DataLine
    {
        public DataFileLine(string data, string comment, Action onModified): base(data)
        {
            this.onModified = onModified;
            Comment = comment;
        }

        public override string Data
        {
            get { return data; }
            set { data = value; if (null != onModified) onModified(); }
        }

        public string Comment { get; private set; }

        private string data;
        private Action onModified;
    }
    #endregion
}
