﻿using System;
using System.Collections.Generic;

namespace Diablo2ZyElRunewords.Entities
{
    public abstract class Rune
    {
        #region public properties
        public string Name{ get; protected set; }

        public int Level{ get; set; }

        public string Comment { get; set; }

        public List<IEffect> Effects{ get; }
        #endregion
    }
}