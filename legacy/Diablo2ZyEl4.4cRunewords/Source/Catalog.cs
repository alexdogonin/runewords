﻿using System.Collections.Generic;

namespace Diablo2ZyElRunewords.Collections
{
    public abstract class Catalog<T> : IEnumerable<T>
    {
        #region public methods
        public abstract T Create(string keyName);

        // public properties
        public abstract T this[string name]{ get; }

        // ICurrentDataBlockProcessor implementation
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return getItems().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return getItems().GetEnumerator();
        }
        #endregion

        #region protect methods
        protected abstract IEnumerable<T> getItems();
        #endregion
    }

    class CatalogByDataFile<T> : Catalog<T>
    {

    }
}