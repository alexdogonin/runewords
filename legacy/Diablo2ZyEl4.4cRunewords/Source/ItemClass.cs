﻿namespace Diablo2ZyElRunewords.Entities
{
    #region 
    public abstract class ItemClass
    {
        #region public properties
        public string ShortName
        { get { return getShortName(); } }

        public string FullName
        {
            get { return getFullName(); }
            set { setFullName(value); }
        }

        public string Comment
        {
            get { return getComment(); }
            set { setComment(value); }
        }
        #endregion

        #region protected methods
        protected abstract string getShortName();
        protected abstract string getFullName();
        protected abstract void setFullName(string name);
        protected abstract string getComment();
        protected abstract void setComment(string comment);
        #endregion
    }
}