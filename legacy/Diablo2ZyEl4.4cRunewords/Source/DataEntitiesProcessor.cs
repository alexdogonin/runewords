﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Diablo2ZyElRunewords
{
    public class DataLineNotEqualMask_Exception: ApplicationException
    {
        public DataLineNotEqualMask_Exception(string line, string pattern, Exception innerException): base(string.Format("Строка \"{1}\" не соответствует шаблону \"{2}\"", line, pattern), innerException) {}
        public DataLineNotEqualMask_Exception(string line, string pattern): this(line, pattern, null) {}
    }

    public class DataEntitiesProcessor: ICurrentDataBlockProcessor
    {
        // public methods
        public DataEntitiesProcessor()
        {
            runes         = new RunesCatalog();
            itemClasses   = new ItemClassesCatalog();
            personClasses = new PersonClassesCatalog();
            runewords     = new RunewordsCollection();
        }

        // interface implementation
        void ICurrentDataBlockProcessor.ProcessDataLines(string dataBlockName, IEnumerable<string> dataLines)
        {
            foreach(string line in dataLines) 
                if (dataBlockName == BLOCKNAME_RUNES)
                    runes.Insert(RunesFactory.CreateRune(line));
                else if (dataBlockName == BLOCKNAME_ITEMCLASSES)
                    itemClasses.Insert(ItemClassFactory.CreateItemClass(line));
                else if (dataBlockName == BLOCKNAME_PERSONCLASES)
                    personClasses.Insert(PersonClassFactory.CreatePersonClass(line));
                else if (dataBlockName == BLOCKNAME_RUNEWORDS)
                    runewords.Insert(RunewordsFactory.CreateRuneword(line, this));
                else
                    throw new Exception("\"" + dataBlockName + "\" неизвестный заголовок");
        }

        IEnumerable<KeyValuePair<string, IEnumerable<string> >> ICurrentDataBlockProcessor.GetDataLines()
        {
            List<KeyValuePair<string, IEnumerable<string> >> dataLines = new List<KeyValuePair<string,IEnumerable<string>>>();

            var linesCollection = new List<string>();
            foreach (var itemClass in itemClasses)
                linesCollection.Add(ItemClassFactory.CreateString(itemClass));
            dataLines.Add( new KeyValuePair<string, IEnumerable<string> >(BLOCKNAME_ITEMCLASSES, linesCollection));

            linesCollection = new List<string>();
            foreach (var personClass in personClasses)
                linesCollection.Add(PersonClassFactory.CreateString(personClass));
            dataLines.Add(new KeyValuePair<string, IEnumerable<string> >(BLOCKNAME_PERSONCLASES, linesCollection));

            linesCollection = new List<string>();
            foreach (var rune in runes)
                linesCollection.Add(RunesFactory.CreateString(rune));
            dataLines.Add(new KeyValuePair<string, IEnumerable<string> >(BLOCKNAME_RUNES, linesCollection));

            linesCollection = new List<string>();
            foreach (var runeword in runewords)
                linesCollection.Add(RunewordsFactory.CreateString(runeword));
            dataLines.Add(new KeyValuePair<string, IEnumerable<string> >(BLOCKNAME_RUNEWORDS, linesCollection));

            return dataLines;
        }

        // public properties
        public ItemClassesCatalog ItemClasses
        {
            get { return itemClasses; }
        }

        public RunesCatalog Runes
        {
            get { return runes; }
        }

        public PersonClassesCatalog PersonClasses
        {
            get { return personClasses; }
        }

        public RunewordsCollection Runewords
        {
            get { return runewords; }
        }

        // private members
        static private string BLOCKNAME_RUNES        = "Runes";
        static private string BLOCKNAME_ITEMCLASSES  = "ItemClasses";
        static private string BLOCKNAME_PERSONCLASES = "PersonClasses";
        static private string BLOCKNAME_RUNEWORDS    = "Runewords";

        private RunesCatalog runes;
        private ItemClassesCatalog itemClasses;
        private PersonClassesCatalog personClasses;
        private RunewordsCollection runewords;
    }

    // private classes
    static class RunesFactory
    {
        static public Rune CreateRune(string dataLine)
        {
            var match = System.Text.RegularExpressions.Regex.Match(dataLine, runeLinePattern);
            if (!match.Success) throw new DataLineNotEqualMask_Exception(dataLine, runeLinePattern);

            var name = match.Groups[1].Value;
            var level = match.Groups[2].Value;
            var comment = match.Groups[4].Value;

            return new Rune(name, int.Parse(level));
        }

        static public string CreateString(Rune rune)
        {
            stringBuilder.Clear();
            stringBuilder.Append(rune.Name).Append(parm_delim).Append(rune.Level);
            return stringBuilder.ToString();
        }

        static private StringBuilder stringBuilder = new StringBuilder(10);
        static private char[] parm_delim = "\t".ToCharArray();
        static private readonly string runeLinePattern = "^(\\S+)\\t(\\d+)(\\t(.+))?$";
    }

    static class ItemClassFactory
    {
        static public ItemClass CreateItemClass(string dataLine)
        {
            var match = System.Text.RegularExpressions.Regex.Match(dataLine, itemClassLinePattern);
            if (!match.Success) throw new DataLineNotEqualMask_Exception(dataLine, itemClassLinePattern);

            var shortName = match.Groups[1].Value;
            var fullName = match.Groups[2].Value;
            var comment = match.Groups[4].Value;

            return new ItemClass(shortName, fullName);
        }

        static public string CreateString(ItemClass itemClass)
        {
            stringBuilder.Clear();
            stringBuilder.Append(itemClass.ShortName).Append(parm_delim).Append(itemClass.FullName);
            return stringBuilder.ToString();
        }

        static private StringBuilder stringBuilder = new StringBuilder(50);
        static private char[] parm_delim = "\t".ToCharArray();
        static private readonly string itemClassLinePattern = "^(\\S+)\\t(.+)(\\t(.+))?$";
    }

    static class PersonClassFactory
    {
        static public PersonClass CreatePersonClass(string dataLine)
        {
            var match = System.Text.RegularExpressions.Regex.Match(dataLine, personClassLinePattern);
            if (!match.Success) throw new DataLineNotEqualMask_Exception(dataLine, personClassLinePattern);

            var shortName = match.Groups[1].Value;
            var fullName = match.Groups[2].Value;
            var comment = match.Groups[4].Value;

            return new PersonClass(shortName, fullName);
        }

        static public string CreateString(PersonClass personClass)
        {
            stringBuilder.Clear();
            stringBuilder.Append(personClass.ShortName).Append(parm_delim).Append(personClass.FullName);
            return stringBuilder.ToString();
        }

        static private StringBuilder stringBuilder = new StringBuilder(50);
        static private char[] parm_delim = "\t".ToCharArray();
        static private readonly string personClassLinePattern = "^(\\S+)\\t(.+)(\\t(.+))?$";
    }

    static class RunewordsFactory
    {
        static public Runeword CreateRuneword(string dataLine, DataEntitiesProcessor processor)
        {
            var match = System.Text.RegularExpressions.Regex.Match(dataLine, runewordLinePattern);
            if (!match.Success) throw new DataLineNotEqualMask_Exception(dataLine, runewordLinePattern);
            
            var runewordName = match.Groups[1].Value;
            var itemClasses = match.Groups[2].Value;
            var level = match.Groups[3].Value;
            var personClasses = match.Groups[4].Value;
            var runes = match.Groups[5].Value;
            var comment = match.Groups[7].Value;

            var itemClassesCollection = new List<ItemClass>();
            var runesCollection       = new List<Rune>();
            var personClassesCollection = new List<PersonClass>(); 

            foreach(string itemClassShortName in itemClasses.Split(classes_delim))
            {
                var itemClass = processor.ItemClasses[itemClassShortName];
                if (itemClass == null) throw new ApplicationException(string.Format("Предмет \"{0}\" не найден.", itemClassShortName));

                itemClassesCollection.Add(itemClass);
            }

            foreach(string runeName in runes.Split(runes_delim))
            {
                var rune = processor.Runes[runeName];
                if (rune == null) throw new ApplicationException(string.Format("Руна \"{0}\" не найдена.", runeName));

                runesCollection.Add(rune);
            }

            foreach(string personClassShortName in personClasses.Split(classes_delim))
            {
                var personClass = processor.PersonClasses[personClassShortName];
                if (personClass == null) throw new ApplicationException(string.Format("Персонаж \"{0}\" не найден.", personClassShortName));

                personClassesCollection.Add(personClass);
            }

            return new Runeword(runewordName, itemClassesCollection, int.Parse(level), personClassesCollection,
                runesCollection, comment);
        }

        static public string CreateString(Runeword runeword)
        {
            StringBuilder stringBuilder = new StringBuilder(200);
            var firstElem = true;

            stringBuilder.Clear();
            stringBuilder.Append(runeword.RunewordName).Append(parms_delim);
            foreach(var itemClass in runeword.ItemClasses)
            {
                if (firstElem) firstElem = false; 
                else  stringBuilder.Append(classes_delim); 
                stringBuilder.Append(itemClass.ShortName);
            }
            stringBuilder.Append(parms_delim).Append(runeword.NeededLevel).Append(parms_delim);
            
            firstElem = true;
            foreach(var personClass in runeword.PersonClasses)
            {
                if (firstElem) firstElem = false; 
                else  stringBuilder.Append(classes_delim); 
                stringBuilder.Append(personClass.ShortName);
            }
            stringBuilder.Append(parms_delim);
            firstElem = true;
            foreach(var rune in runeword.Runes)
            {
                if (firstElem) firstElem = false; 
                else stringBuilder.Append(runes_delim); 
                stringBuilder.Append(rune.Name);
            }
            if (runeword.Comment.Length > 0)
                stringBuilder.Append(parms_delim).Append(runeword.Comment);

            return stringBuilder.ToString();
        }

        static private StringBuilder stringBuilder = new StringBuilder(200);

        static private char[] parms_delim = "\t".ToCharArray();
        static private char[] runes_delim = "-".ToCharArray();
        static private char[] classes_delim = ",".ToCharArray();

        static private readonly string runewordLinePattern = "^(.+)\\t(\\S+)\\t(\\d+)\\t(\\S+)\\t(\\S+)(\\t(.+))?$";
    }
}