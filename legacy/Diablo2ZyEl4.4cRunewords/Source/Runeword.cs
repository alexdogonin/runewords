﻿using System.Collections.Generic;

namespace Diablo2ZyElRunewords.Entities
{
    public abstract class Runeword
    {
        #region public properties
        public string RunewordName{
            get { return getName(); }
            set { setName(value); }
        }

        public int SocketsCount
        { get { return Runes.Count; } }

        public string Comment{
            set { setComment(value); }
            get { return getComment(); }
        }

        public int NeededLevel
        { get { return Runes.Max(rune => rune.Level); } }

        public List<ItemClass> ItemClasses
        { get { return getItemClasses(); } }

        public List<PersonClass> PersonClasses
        { get { return getPersonClasses(); } }

        public List<Rune> Runes
        { get { return getRunes(); } }
        #endregion

        #region private methods
        protected abstract string getName();
        protected abstract void setName(string name);
        protected abstract string getComment();
        protected abstract void setComment(string comment);
        protected abstract List<ItemClass> getItemClasses();
        protected abstract List<PersonClass> getPersonClasses();
        protected abstract List<Rune> getRunes();
        #endregion
    }
}