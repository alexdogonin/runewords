﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RegularExpressions = System.Text.RegularExpressions;

namespace Diablo2ZyElRunewords
{
    public struct ValuesRange
    {
        public int MinValue, MaxValue;

        public ValuesRange(int min, int max)
        {
            MinValue = min;
            MaxValue = max;
        }

        public bool Include(int value)
        {
            return (MinValue <= value) && (value <= MaxValue);
        }

        public bool Include(ValuesRange range)
        {
            return Include(range.MinValue) && Include(range.MaxValue);
        }

        public bool Intersect(ValuesRange range)
        {
            return !Include(range) && (Include(range.MinValue) || Include(range.MaxValue));
        }

        public override bool Equals(object obj)
        {
            ValuesRange? range = obj as ValuesRange?;
            return ((null != range) && (this == range)) || ((null == range) && base.Equals(obj));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        static public bool operator==(ValuesRange range1, ValuesRange range2)
        {
            return (range1.MinValue == range2.MinValue) && (range1.MaxValue == range2.MaxValue);
        }

        static public bool operator!=(ValuesRange range1, ValuesRange range2)
        {
            return !(range1 == range2);
        }
    }

    public class RunewordFilterNode
    {
        // public methods
        public RunewordFilterNode()
        {
            NameRegExPattern = "";
            ItemClasses      = new ItemClassesCatalog();
            PersonClasses    = new PersonClassesCatalog();
            Runes            = new RunesCatalog();
            LevelRange       = new ValuesRange(0, RUNE_MAX_LEVEL);
            SocketsCount     = new List<int>(5);
        }

        public RunewordFilterNode(RunewordFilterNode filterNode)
        {
            NameRegExPattern = filterNode.NameRegExPattern.ToString();
            ItemClasses      = new ItemClassesCatalog(filterNode.ItemClasses);
            PersonClasses    = new PersonClassesCatalog(filterNode.PersonClasses);
            Runes            = new RunesCatalog(filterNode.Runes);
            LevelRange       = filterNode.LevelRange;
            SocketsCount     = new List<int>(5);
        }

        public bool CheckRuneword(Runeword runeword)
        {
            return ((runeword.RunewordName.Length == 0) ||
                RegularExpressions.Regex.IsMatch(runeword.RunewordName, this.NameRegExPattern, RegularExpressions.RegexOptions.IgnoreCase)) &&

                ( (LevelRange == new ValuesRange()) ||  LevelRange.Include(runeword.NeededLevel)) &&

                ((SocketsCount.Count == 0) || SocketsCount.Contains(runeword.SocketsCount)) &&
            
                ((ItemClasses.Count == 0) || (ItemClasses.Intersect(runeword.ItemClasses).Count() > 0)) &&

                ((PersonClasses.Count == 0) || (PersonClasses.Intersect(runeword.PersonClasses).Count() > 0)) &&

                ((Runes.Count == 0) || (Runes.Intersect(runeword.Runes).Count() == Runes.Count));
        }

        // public properties
        public string NameRegExPattern
        { get; set; }

        public ValuesRange LevelRange
        { get; set; }

        public ItemClassesCatalog ItemClasses
        { get; private set; }

        public PersonClassesCatalog PersonClasses
        { get; private set; }

        public RunesCatalog Runes
        { get; private set; }

        public List<int> SocketsCount
        { get; private set; }

        // private members
        private const int RUNE_MAX_LEVEL = 255;
    }

    public class DataFilter
    {
        public static IEnumerable<Runeword> SelectRunewords(IEnumerable<Runeword> runewords, RunewordFilterNode etalon)
        {
            return runewords.Where(runeword => etalon.CheckRuneword(runeword));
        }
    }
}