﻿namespace Diablo2ZyElRunewords
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param Name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.toolStripFilterButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMainMenu = new System.Windows.Forms.MenuStrip();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.queryLinesCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listViewRunewords = new System.Windows.Forms.ListView();
            this.columnRuneWordName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSockets = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnItemsClasses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNeededLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeroClasses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRuneword = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new Diablo2ZyElRunewords.DoubleBufferedPanel();
            this.runewordInfoLabel = new System.Windows.Forms.Label();
            this.toolBar.SuspendLayout();
            this.menuMainMenu.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolBar
            // 
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolBar.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFilterButton,
            this.toolStripSeparator1});
            this.toolBar.Location = new System.Drawing.Point(0, 27);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(25, 538);
            this.toolBar.TabIndex = 0;
            this.toolBar.Text = "toolStrip1";
            // 
            // toolStripFilterButton
            // 
            this.toolStripFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.toolStripFilterButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripFilterButton.Image = global::Diablo2ZyElRunewords.Properties.Resources.Filter;
            this.toolStripFilterButton.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripFilterButton.Name = "toolStripFilterButton";
            this.toolStripFilterButton.Size = new System.Drawing.Size(22, 24);
            this.toolStripFilterButton.Text = "Фильтр";
            this.toolStripFilterButton.Click += new System.EventHandler(this.toolStripFilterButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(22, 6);
            // 
            // menuMainMenu
            // 
            this.menuMainMenu.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.PlansToolStripMenuItem});
            this.menuMainMenu.Location = new System.Drawing.Point(0, 0);
            this.menuMainMenu.Name = "menuMainMenu";
            this.menuMainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuMainMenu.Size = new System.Drawing.Size(1054, 27);
            this.menuMainMenu.TabIndex = 5;
            this.menuMainMenu.Text = "menuStrip1";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(71, 23);
            this.closeToolStripMenuItem.Text = "Закрыть";
            this.closeToolStripMenuItem.Visible = false;
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.aboutToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.aboutToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 23);
            this.aboutToolStripMenuItem.Text = "О программе";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // PlansToolStripMenuItem
            // 
            this.PlansToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.PlansToolStripMenuItem.Name = "PlansToolStripMenuItem";
            this.PlansToolStripMenuItem.Size = new System.Drawing.Size(129, 23);
            this.PlansToolStripMenuItem.Text = "Планировалось...";
            this.PlansToolStripMenuItem.Click += new System.EventHandler(this.PlansToolStripMenuItem_Click);
            // 
            // statusBar
            // 
            this.statusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.queryLinesCount});
            this.statusBar.Location = new System.Drawing.Point(0, 565);
            this.statusBar.Margin = new System.Windows.Forms.Padding(3);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1054, 22);
            this.statusBar.TabIndex = 10;
            this.statusBar.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(150, 17);
            this.toolStripStatusLabel1.Text = "Число строк: ";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // queryLinesCount
            // 
            this.queryLinesCount.AutoSize = false;
            this.queryLinesCount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.queryLinesCount.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.queryLinesCount.Name = "queryLinesCount";
            this.queryLinesCount.Size = new System.Drawing.Size(50, 17);
            this.queryLinesCount.Text = "0";
            this.queryLinesCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(25, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1029, 538);
            this.panel2.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listViewRunewords);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(648, 538);
            this.panel3.TabIndex = 15;
            // 
            // listViewRunewords
            // 
            this.listViewRunewords.AutoArrange = false;
            this.listViewRunewords.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnRuneWordName,
            this.columnSockets,
            this.columnItemsClasses,
            this.columnNeededLevel,
            this.columnHeroClasses,
            this.columnRuneword});
            this.listViewRunewords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewRunewords.FullRowSelect = true;
            this.listViewRunewords.HideSelection = false;
            this.listViewRunewords.LabelEdit = true;
            this.listViewRunewords.Location = new System.Drawing.Point(0, 0);
            this.listViewRunewords.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listViewRunewords.MultiSelect = false;
            this.listViewRunewords.Name = "listViewRunewords";
            this.listViewRunewords.OwnerDraw = true;
            this.listViewRunewords.Size = new System.Drawing.Size(648, 538);
            this.listViewRunewords.TabIndex = 15;
            this.listViewRunewords.UseCompatibleStateImageBehavior = false;
            this.listViewRunewords.View = System.Windows.Forms.View.Details;
            this.listViewRunewords.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewRunewords_ItemSelectionChanged);
            this.listViewRunewords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listViewRunewords_KeyPress);
            // 
            // columnRuneWordName
            // 
            this.columnRuneWordName.Text = "Название";
            this.columnRuneWordName.Width = 126;
            // 
            // columnSockets
            // 
            this.columnSockets.Text = "Число сокетов";
            // 
            // columnItemsClasses
            // 
            this.columnItemsClasses.Text = "Классы вещей";
            this.columnItemsClasses.Width = 101;
            // 
            // columnNeededLevel
            // 
            this.columnNeededLevel.Text = "Уровень";
            this.columnNeededLevel.Width = 58;
            // 
            // columnHeroClasses
            // 
            this.columnHeroClasses.Text = "Класс героя";
            this.columnHeroClasses.Width = 89;
            // 
            // columnRuneword
            // 
            this.columnRuneword.Text = "Рунное слово";
            this.columnRuneword.Width = 192;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(648, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 538);
            this.splitter1.TabIndex = 13;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.runewordInfoLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(653, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 100);
            this.panel1.Size = new System.Drawing.Size(376, 538);
            this.panel1.TabIndex = 12;
            // 
            // runewordInfoLabel
            // 
            this.runewordInfoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.runewordInfoLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.runewordInfoLabel.Location = new System.Drawing.Point(5, 5);
            this.runewordInfoLabel.Name = "runewordInfoLabel";
            this.runewordInfoLabel.Size = new System.Drawing.Size(366, 433);
            this.runewordInfoLabel.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1054, 587);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.menuMainMenu);
            this.Controls.Add(this.statusBar);
            this.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menuMainMenu;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.Text = "Рунные слова";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrame_FormClosing);
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.menuMainMenu.ResumeLayout(false);
            this.menuMainMenu.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.MenuStrip menuMainMenu;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripFilterButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel queryLinesCount;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter1;
        private DoubleBufferedPanel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView listViewRunewords;
        private System.Windows.Forms.ColumnHeader columnRuneWordName;
        private System.Windows.Forms.ColumnHeader columnSockets;
        private System.Windows.Forms.ColumnHeader columnItemsClasses;
        private System.Windows.Forms.ColumnHeader columnNeededLevel;
        private System.Windows.Forms.ColumnHeader columnHeroClasses;
        private System.Windows.Forms.ColumnHeader columnRuneword;
        private System.Windows.Forms.Label runewordInfoLabel;
        private System.Windows.Forms.ToolStripMenuItem PlansToolStripMenuItem;
    }
}

