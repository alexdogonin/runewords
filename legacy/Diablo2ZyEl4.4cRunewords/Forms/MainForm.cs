﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Diablo2ZyElRunewords.Controls;

using GenericCollections = System.Collections.Generic;

namespace Diablo2ZyElRunewords
{
    public partial class MainForm : Form
    {
        #region public methods
        public MainForm()
        {
            InitializeComponent();            

            runewordsFilterControl = new RunewordFilterControl();
            donatFrame = new Diablo2ZyElRunewords.Controls.DonatFrame();

            runewordsFilterControl.Hide();
            runewordsFilterControl.Accept += filterApplyClick;
            

            donatFrame.Hide();
            donatFrame.Size = new Size(330, 130);
            this.Controls.Add(runewordsFilterControl);
            this.Controls.Add(donatFrame);

            this.Icon = Icon.FromHandle (Diablo2ZyElRunewords.Properties.Resources.icon.GetHicon());
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.DoubleBuffered = true;
            this.isDonatFrameShown = false;
            this.startProgramTime = DateTime.Now;

            dataFile = new DataFileManager(DataFileConsts.DataFileName);
            processor = new DataEntitiesProcessor();
            dataFile.SetDataProcessor(processor);
            dataFile.ParseDataFile();
            

            fillListViewRunewords(processor.Runewords);
            //setAllChildControllsClickEventHandler(this);

            this.listViewRunewords.ColumnClick      += listViewRunewords_ColumnClick;
            // для отрисовки на столбцах направления сортировки
            this.listViewRunewords.DrawColumnHeader += RunewordsListViewRenderer.DrawColumnHeader;
            this.listViewRunewords.DrawItem         += RunewordsListViewRenderer.DrawItem;
            this.listViewRunewords.DrawSubItem      += RunewordsListViewRenderer.DrawSubitem;


            // для закрывания фильтра при нажатии мимо него
            this.listViewRunewords.MouseDown += anyControl_MouseDown;
            this.menuMainMenu.MouseDown      += anyControl_MouseDown;
            this.toolBar.MouseDown           += anyControl_MouseDown;
            this.runewordInfoLabel.MouseDown += anyControl_MouseDown;
            this.panel1.MouseDown            += anyControl_MouseDown;
            this.statusBar.MouseDown         += anyControl_MouseDown;

            runewordsFilterControl.RunewordsDataSource = processor.Runewords;
            runewordsFilterControl.ItemClassesDataSource = processor.ItemClasses;
            runewordsFilterControl.PersonClassesDataSource = processor.PersonClasses;
            runewordsFilterControl.RunesDataSource = processor.Runes;
        }

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static public void Main(string[] consoleParms)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        #endregion

        #region private methods
        private void filterApplyClick(object sender, EventArgs e)
        {
            
            fillListViewRunewords(DataFilter.SelectRunewords(processor.Runewords, runewordsFilterControl.FilterNode));
            runewordsFilterControl.Hide();
        }

        private bool isPlacedInControl(Control checkedControl, Control containerControl)
        {
            foreach(Control child in containerControl.Controls)
            {
                if(child.Equals(checkedControl) || isPlacedInControl(checkedControl, child))
                return true;
            }

            return false;
        }

        private void fillListViewRunewords(IEnumerable<Runeword> collection)
        {
            listViewRunewords.Items.Clear();

            foreach (Runeword runeword in collection)
            {
                ListViewItem listItem = new ListViewItem();
                listItem.Text = runeword.RunewordName;
                listItem.SubItems.Add(runeword.SocketsCount.ToString());
                listItem.SubItems.Add(runeword.ItemClassesString);
                listItem.SubItems.Add(runeword.NeededLevel.ToString());
                listItem.SubItems.Add(runeword.HeroClassesString);
                listItem.SubItems.Add(runeword.RunesString);
                listItem.Tag = runeword;
                listViewRunewords.Items.Add(listItem);
            }

            queryLinesCount.Text = listViewRunewords.Items.Count.ToString();
        }

        //private void setAllChildControllsClickEventHandler(Control parentControl)
        //{
        //    foreach (Control childControl in parentControl.Controls)
        //    {
        //        childControl.Click += anyControlInFormClick;
        //        setAllChildControllsClickEventHandler(childControl);
        //    }
        //}

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
            about.Dispose();
        }

        private void toolStripFilterButton_Click(object sender, EventArgs e)
        {
            if (runewordsFilterControl.Visible)
                ;//runewordsFilterControl.Hide();
            else{
                runewordsFilterControl.Location = new Point((this.Size.Width - runewordsFilterControl.Size.Width)/2,
                    (this.Size.Height - runewordsFilterControl.Size.Height)/2);
                runewordsFilterControl.Show();
                runewordsFilterControl.BringToFront();
            }
        }
        
        private void listViewRunewords_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var sorter = listViewRunewords.ListViewItemSorter as RunewordsListViewItemComparer;
            if (sorter == null ||
                sorter.SortedColumnIndex != e.Column)
            {
                listViewRunewords.ListViewItemSorter = new RunewordsListViewItemComparer(e.Column);
                listViewRunewords.Invalidate(true);
                listViewRunewords.Sort();
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void splitter1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(SystemPens.ControlDark, new Point(splitter1.Width/2, splitter1.Height/5), 
                new Point(splitter1.Width/2, splitter1.Height - splitter1.Height/5));
        }

        private void MainFrame_FormClosing(object sender, FormClosingEventArgs e)
        {
            var rand = new Random();
            if (!isDonatFrameShown && 
                (((startProgramTime - DateTime.Now).Minutes >= 30 && rand.Next(0, 100) > 20) || rand.Next(0, 100) > 80))
            {
                donatFrame.Location = new Point((this.Size.Width - donatFrame.Size.Width)/2,
                                                (this.Size.Height - donatFrame.Size.Height)/2);
                donatFrame.Show();
                donatFrame.BringToFront();

                isDonatFrameShown = true;
                e.Cancel = true;
            }
        }

        private void listViewRunewords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
                listViewRunewords.SelectedIndices.Clear();
        }

        private void listViewRunewords_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.Item != null)
            {
                Runeword curRuneword = e.Item.Tag as Runeword;
                if (curRuneword != null)
                {
                    runewordInfoLabel.Text = RunewordsToTextConverter.GetAsString(curRuneword);
                    return;
                }
            }
            
            runewordInfoLabel.Text = "";
        }

        private void PlansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var plansForm = new PlansForm();
            plansForm.ShowDialog(this);
            plansForm.Dispose();
        }

        private void anyControl_MouseDown(object sender, EventArgs e)
        {
            if (runewordsFilterControl.Visible)
                runewordsFilterControl.Hide();
            if (donatFrame.Visible)
                donatFrame.Hide();
        }
        #endregion

        #region private members
        //private int runewordsFilterFrameHeight;
        //private RunewordsCollection allRunewords;
        //private RunesCatalog runes;
        private RunewordFilterControl runewordsFilterControl;
        private Controls.DonatFrame donatFrame;
        private DateTime startProgramTime;
        private bool isDonatFrameShown;

        private DataFileManager dataFile;
        private DataEntitiesProcessor processor;
        #endregion
    }

    #region additional classes
    // Класс нужен для сортировки элементов в списке ListView
    class RunewordsListViewItemComparer : System.Collections.IComparer
    {
        #region public methods

        public RunewordsListViewItemComparer() { columnIndex = 0; isMayBeNumericColumn = true; }
        public RunewordsListViewItemComparer(int sortedColumnIndex) 
        { columnIndex = sortedColumnIndex; isMayBeNumericColumn = true; }
        public RunewordsListViewItemComparer(int sortedColumnIndex, bool numericColumn) 
        { columnIndex = sortedColumnIndex; isMayBeNumericColumn = numericColumn; }
        
        #endregion

        #region public properties

        public int SortedColumnIndex
        {
            get {return this.columnIndex;}
        }

        #endregion
        
        #region private methods
        int System.Collections.IComparer.Compare(object item1obj, object item2obj)
        {
            ListViewItem item1 = (ListViewItem)item1obj, item2 = (ListViewItem)item2obj;
            string string1, string2;
            if (columnIndex == 0)
            {
                string1 = item1.Text;
                string2 = item2.Text;
            }
            else
            {
                string1 = item1.SubItems[columnIndex].Text;
                string2 = item2.SubItems[columnIndex].Text;
            }

            double item1Val, item2Val;
            int copmResult = 0;
            if (isMayBeNumericColumn && Double.TryParse(string1, out item1Val) && Double.TryParse(string2, out item2Val))
                copmResult = item1Val < item2Val ? -1 :
                    item1Val > item2Val ? 1 : 0;
            else
                copmResult = String.Compare(string1, string2);
            return copmResult;
        }
        #endregion

        #region private members
        private int columnIndex;
        private bool isMayBeNumericColumn;
        #endregion
    }

    class RunewordsListViewRenderer
    {
        public static void DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {           
            var sorter = e.Header.ListView.ListViewItemSorter as RunewordsListViewItemComparer;
            
            if (sorter != null && sorter.SortedColumnIndex == e.ColumnIndex)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                e.DrawBackground();
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(20, Color.Blue)), e.Bounds);
                e.DrawText(TextFormatFlags.VerticalCenter|TextFormatFlags.EndEllipsis|TextFormatFlags.Left|TextFormatFlags.NoPadding|TextFormatFlags.WordEllipsis);

                int sortedColInd = sorter.SortedColumnIndex;
            
                drawTriangleArrowDown(new Point((int)(e.Bounds.Right - triangleArrow.Width - 4), (int)(e.Bounds.Bottom / 2 - triangleArrow.Height/2)), e.Graphics);
            }
            else
                e.DrawDefault = true;
        }

        public static void DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        public static void DrawSubitem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private static void drawTriangleArrowDown(Point position, Graphics e)
        {
            e.TranslateTransform(position.X, position.Y);
            e.DrawPolygon(Pens.Black, triangleArrow.Points);
        }

        static private Polygon triangleArrow = new Polygon(new Point[]{
                                                    new Point(5, 1),
                                                    new Point(9, 6),
                                                    new Point(1, 6)
                                                });
    }

    class Polygon
    {
        public Polygon(Point[] points)
        {
            width = height = 0;
            this.points = points;
            foreach(Point point in points)
            {
                if (width < point.X) width = point.X;
                if (height < point.Y) height = point.Y;
            }
        }

        public Point[] Points
        {
            get{return points;}
        }

        public float Width
        {
            get {return width;}
        }

        public float Height
        {
            get {return height;}
        }

        private Point[] points;
        private float width, height;
    }

    class RunewordsToTextConverter
    {
        static public string GetAsString(Runeword runeword)
        {
            var isFirstIter = true;
            runewordToTextStrBuilder.Clear();
            runewordToTextStrBuilder.AppendLine("Название:").AppendLine(runeword.RunewordName).AppendLine();
            runewordToTextStrBuilder.Append("Сокетов: ").Append(runeword.SocketsCount).AppendLine();
            runewordToTextStrBuilder.Append("Уровень: ").Append(runeword.NeededLevel).AppendLine().AppendLine();
            
            runewordToTextStrBuilder.Append("Классы вещей: ");
            foreach(ItemClass item in runeword.ItemClasses)
            {
                if (!isFirstIter) runewordToTextStrBuilder.Append(", ");
                else isFirstIter = false;
                runewordToTextStrBuilder.Append(item.FullName);
            }
            runewordToTextStrBuilder.AppendLine().AppendLine();
            
            runewordToTextStrBuilder.Append("Класс персонажа: ");
            isFirstIter = true;
            foreach(PersonClass person in runeword.PersonClasses)
            {
                if (!isFirstIter) runewordToTextStrBuilder.Append(", ");
                else isFirstIter = false;
                runewordToTextStrBuilder.Append(person.FullName);
            }
            runewordToTextStrBuilder.AppendLine().AppendLine();

            runewordToTextStrBuilder.Append("Руны: ");
            isFirstIter = true;
            foreach(Rune rune in runeword.Runes)
            {
                if (!isFirstIter) runewordToTextStrBuilder.Append(" - ");
                else isFirstIter = false;
                runewordToTextStrBuilder.Append(rune.Name);
            }

            return runewordToTextStrBuilder.ToString();
        }

        static private StringBuilder runewordToTextStrBuilder = new StringBuilder();
    }

    struct MARGINS 
    {
        //public MARGINS()
        //{
        //    leftWidth = 0;
        //    bottomWidth = 0;
        //    topWidth = 0;
        //    rightWidth = 0;
        //}

        public int LeftWidth
        {
            get { return leftWidth; }
            set { leftWidth = value; }
        }
        
        public int WightWidth
        {
            get { return rightWidth; }
            set { rightWidth = value; }
        }
        
        public int TopWidth
        {
            get { return topWidth; }
            set { topWidth = value; }
        }
        
        public int BottomWidth
        {
            get { return bottomWidth; }
            set { bottomWidth = value; }
        }

        [System.Runtime.InteropServices.DllImport("Dwmapi.dll", EntryPoint = "DwmExtendFrameIntoClientArea")]
        static public extern int DwmExtendFrameIntoClientArea(IntPtr wnd, IntPtr margins);
        
        int leftWidth;
        int rightWidth;
        int topWidth;
        int bottomWidth;
    }
    #endregion
}
