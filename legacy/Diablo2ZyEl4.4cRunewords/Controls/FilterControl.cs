﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Diablo2ZyElRunewords.Controls;

namespace Diablo2ZyElRunewords
{
    // TODO: сделать весь фильтер на контроле Popup
    public partial class RunewordFilterControl : UserControl
    {
        #region public methods
        public RunewordFilterControl()
        {
            InitializeComponent();

            bindedControls = new BindedControlsCollection();
            bindedControls.Add(runewordNameButton, runewordNamePanel);
            bindedControls.Add(holeCountButton,    holeCountPanel);
            bindedControls.Add(levelButton,        levelPanel);
            bindedControls.Add(personClassButton,  personClassPanel);
            bindedControls.Add(itemClassButton,    itemClassPanel);
            bindedControls.Add(runesButton,        runesPanel);
            
            runewordNameButton.IsCheckedChange += CheckedButton_IsCheckedChange;
            holeCountButton.IsCheckedChange    += CheckedButton_IsCheckedChange;
            levelButton.IsCheckedChange        += CheckedButton_IsCheckedChange;
            personClassButton.IsCheckedChange  += CheckedButton_IsCheckedChange;
            itemClassButton.IsCheckedChange    += CheckedButton_IsCheckedChange;
            runesButton.IsCheckedChange        += CheckedButton_IsCheckedChange;
            
            currentFilterNode = new RunewordFilterNode();
            defaultFilterNode = new RunewordFilterNode();
            previousFilterNode = new RunewordFilterNode();// планировал использовать его для подсветки изменений в фильтре

            greaterLevelLabel.Text = runewordsLevelTrackbar.GreaterValue.ToString();
            smallerLevelLabel.Text = runewordsLevelTrackbar.SmallerValue.ToString();

            runewordsNamesDataSource = new AutoCompleteStringCollection();
            runewordNameTextBox.AutoCompleteCustomSource = runewordsNamesDataSource;
            runewordNameTextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            runewordNameTextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }
        #endregion

        #region public properties
        public RunewordFilterNode FilterNode
        {
            get { return currentFilterNode; }
            set { currentFilterNode = value; }
        }

        public RunewordsCollection RunewordsDataSource
        {
            get {return runewordsDataSource;}
            set { runewordsDataSource = value;
                updateRunewordsNamesDataSource();
            }
        }

        public RunesCatalog RunesDataSource
        {
            get {return runesDataSource;}
            set {runesDataSource = value;
                runesControl.Items.Clear();
                foreach(Rune rune in runesDataSource)
                {
                    var item = new TileComboBoxItem(rune.Name);
                    item.Tag = rune;
                    runesControl.Items.Add(item);
                }
            }
        }

        public PersonClassesCatalog PersonClassesDataSource
        {
            get {return personClassesDataSource;}
            set {personClassesDataSource = value;
                personClassesControl.Items.Clear();
                foreach(PersonClass personClass in personClassesDataSource)
                {
                    var item = new TileComboBoxItem(personClass.FullName);
                    item.Tag = personClass;
                    personClassesControl.Items.Add(item);
                }
            }
        }

        public ItemClassesCatalog ItemClassesDataSource
        {
            get {return itemClassesDataSource;}
            set {itemClassesDataSource = value;
                itemClassesControl.Items.Clear();
                foreach(ItemClass itemClass in itemClassesDataSource)
                {
                    var item = new TileComboBoxItem(itemClass.FullName);
                    item.Tag = itemClass;
                    itemClassesControl.Items.Add(item);
                }
            }
        }
        #endregion

        #region protected methods
        protected void Raise_AcceptEvent()
        {
            if (Accept != null)
                Accept(this, new EventArgs());
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams parameters = base.CreateParams;
                parameters.ExStyle |= 0x00050001;
                return parameters;
            }
        }
        #endregion

        #region private event handlers
        private void filterFild_SizeChanged(object sender, EventArgs e)
        {
            Control bindedControl, thisControl = sender as Control;
            if (bindedControls.TryGetBindedControl(thisControl, out bindedControl))
            {
                thisControl.SuspendLayout();
                bindedControl.Height = thisControl.Height;
                thisControl.ResumeLayout();
            }
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            controlToFilterNode();
 
            Raise_AcceptEvent();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            currentFilterNode = new RunewordFilterNode(defaultFilterNode);
            filterNodeToControl();
        }

        private void levelTwoSidesTrackBar_GreaterValueChanged(object sender, EventArgs e)
        {
            greaterLevelLabel.Text = runewordsLevelTrackbar.GreaterValue.ToString();
        }

        private void levelTwoSidesTrackBar_SmallerValueChanged(object sender, EventArgs e)
        {
            smallerLevelLabel.Text = runewordsLevelTrackbar.SmallerValue.ToString();
        }

        private void CheckedButton_IsCheckedChange(object sender, EventArgs e)
        {
            Control panel;
            if (bindedControls.TryGetBindedControl(sender as Control, out panel))
                panel.Visible = ((Controls.CheckedButton)sender).IsChecked;
        }
        #endregion

        #region private methods

        private void updateRunewordsNamesDataSource()
        {
            runewordsNamesDataSource.Clear();
            foreach(Runeword runeword in runewordsDataSource)
                runewordsNamesDataSource.Add(runeword.RunewordName);
        }

        private void controlToFilterNode()
        {
            if (runewordNameButton.IsChecked)
                currentFilterNode.NameRegExPattern = runewordNameTextBox.Text;
            else
                currentFilterNode.NameRegExPattern = "";
            
            currentFilterNode.SocketsCount.Clear();
            if (holeCountButton.IsChecked)
            {
                if (checkBox1.Checked)
                    currentFilterNode.SocketsCount.Add(2);
                if (checkBox2.Checked)
                    currentFilterNode.SocketsCount.Add(3);
                if (checkBox3.Checked)
                    currentFilterNode.SocketsCount.Add(4);
                if (checkBox4.Checked)
                    currentFilterNode.SocketsCount.Add(5);
                if (checkBox5.Checked)
                    currentFilterNode.SocketsCount.Add(6);
            }

            currentFilterNode.LevelRange = levelButton.IsChecked ? new ValuesRange(runewordsLevelTrackbar.SmallerValue, runewordsLevelTrackbar.GreaterValue) :
                new ValuesRange();;

#warning использование поля tag может привести к ошибке
            currentFilterNode.PersonClasses.Clear();
            if (personClassButton.IsChecked)
                foreach(TileComboBoxItem item in personClassesControl.SelectedItems)
                    currentFilterNode.PersonClasses.Insert((PersonClass)item.Tag);

            currentFilterNode.ItemClasses.Clear();
            if (itemClassButton.IsChecked)
                foreach(TileComboBoxItem item in itemClassesControl.SelectedItems)
                    currentFilterNode.ItemClasses.Insert((ItemClass)item.Tag);

            currentFilterNode.Runes.Clear();
            if (runesButton.IsChecked)
                foreach(TileComboBoxItem item in runesControl.SelectedItems)
                    currentFilterNode.Runes.Insert((Rune)item.Tag);
        }

        private void filterNodeToControl()
        {
            runewordNameTextBox.Text = runewordNameButton.IsChecked ? currentFilterNode.NameRegExPattern : "";

            if (holeCountButton.IsChecked)
            {
                checkBox1.Checked = currentFilterNode.SocketsCount.Contains(2);
                checkBox2.Checked = currentFilterNode.SocketsCount.Contains(3);
                checkBox3.Checked = currentFilterNode.SocketsCount.Contains(4);
                checkBox4.Checked = currentFilterNode.SocketsCount.Contains(5);
                checkBox5.Checked = currentFilterNode.SocketsCount.Contains(6);
            }

            if (levelButton.IsChecked)
            {
                runewordsLevelTrackbar.SmallerValue = currentFilterNode.LevelRange.MinValue;
                runewordsLevelTrackbar.GreaterValue = currentFilterNode.LevelRange.MaxValue;
            }

            // TODO: это очень не оптимально и вообще позорно
            if (personClassButton.IsChecked)
            {
                personClassesControl.SelectedItems.Clear();
                foreach(PersonClass personClass in currentFilterNode.PersonClasses)
                    foreach(TileComboBoxItem item in personClassesControl.Items)
                        if ((PersonClass)item.Tag == personClass)
                        {
                            personClassesControl.SelectedItems.Add(item);
                            break;
                        }
            }

            if (itemClassButton.IsChecked)
            {
                itemClassesControl.SelectedItems.Clear();
                foreach(ItemClass itemClass in currentFilterNode.ItemClasses)
                    foreach(TileComboBoxItem item in itemClassesControl.Items)
                        if ((ItemClass)item.Tag == itemClass)
                        {
                            itemClassesControl.SelectedItems.Add(item);
                            break;
                        }
            }

            if (runesButton.IsChecked)
            {
                runesControl.SelectedItems.Clear();
                foreach(Rune rune in currentFilterNode.Runes)
                    foreach(TileComboBoxItem item in runesControl.Items)
                        if ((Rune)item.Tag == rune)
                        {
                            runesControl.SelectedItems.Add(item);
                            break;
                        }
            }
        }

        #endregion

        #region private members
        public event EventHandler Accept;
        public event EventHandler RelativeLocationChanged;
        public event EventHandler FilterNodeChanged;

        private BindedControlsCollection bindedControls;
        private RunewordFilterNode currentFilterNode;
        private RunewordFilterNode previousFilterNode;
        private RunewordFilterNode defaultFilterNode;

        private bool isMoving;
        private Point movingStartPosition;

        private AutoCompleteStringCollection runewordsNamesDataSource;

        private RunesCatalog runesDataSource;
        private RunewordsCollection runewordsDataSource;
        private ItemClassesCatalog itemClassesDataSource;
        private PersonClassesCatalog personClassesDataSource;
        #endregion
    }

    // private classes
    class BindedControlsCollection
    {
        public BindedControlsCollection() { bindedcontrols = new List<ControlPair>(); }

        public void Add(Control first, Control second)
        { bindedcontrols.Add(new ControlPair(first, second)); }

        public void Add(ControlPair pair)
        { bindedcontrols.Add(pair); }

        public bool TryGetBindedControl(Control control, out Control bindedControl)
        {
            foreach(ControlPair pair in bindedcontrols)
                if (pair.First == control)
                {
                    bindedControl = pair.Second;
                    return true;
                }
                else if (pair.Second == control)
                {
                    bindedControl = pair.First;
                    return true;
                }
            bindedControl = null;

            return false;
        }

        List<ControlPair> bindedcontrols;
    }

    class ControlPair
    {
        public ControlPair(Control first, Control second)
        {
            this.first = first;
            this.second = second;
        }

        public Control Second
        {
            get { return second; }
        }

        public Control First
        {
            get { return first; }
        }

        private Control first, second;
    }
}
