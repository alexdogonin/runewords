﻿/*
 * TODO: Реализовать кнопку с возможностью выбора, наследуясь от ButtonBase
 * 
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords.Controls
{
    public class CheckedButton: Control
    {
        public CheckedButton(): base()
        {
            this.Width = 100;
            this.Height = 40;       
            this.DoubleBuffered = true;
        }

        public ContentAlignment TextAlign
        {
            get { return textAlign; }
            set { textAlign = value; this.Invalidate(); }
        }

        public Color MouseOverColor
        {
            get{ return ((SolidBrush)mouseOverBrush).Color; }
            set{ mouseOverBrush = new SolidBrush(value); this.Invalidate();}
        }

        public Color MouseDownColor
        {
            get{ return ((SolidBrush)mouseDownBrush).Color; }
            set{ mouseDownBrush = new SolidBrush(value); this.Invalidate();}
        }

        public Color CheckedBackColor
        {
            get{ return ((SolidBrush)checkedBackBrush).Color; }
            set{ checkedBackBrush = new SolidBrush(value); this.Invalidate();}
        }

        public Color ChechedFontColor
        {
            get{ return ((SolidBrush)checkedForeBrush).Color; }
            set{ checkedForeBrush = new SolidBrush(value); this.Invalidate();}
        }

        public Color ChechedBorderColor
        {
            get{ return ((Pen)checkedBorderPen).Color; }
            set{ checkedBorderPen = new Pen(value, checkedBorderWidth); this.Invalidate();}
        }

        public int CheckedBorderWidth
        {
            get{return checkedBorderWidth;}
            set{checkedBorderWidth = value; checkedBorderPen = new Pen(checkedBorderPen.Color, value); this.Invalidate();}
        }

        public Color BorderColor
        {
            get{ return borderPen.Color; }
            set{ borderPen = new Pen(value); this.Invalidate();}
        }

        public bool IsChecked
        {
            get{ return isCheckedFlag; }
            set{ isCheckedFlag = value; Raise_IsCheckedChange(); this.Invalidate(); }
        }

        public event EventHandler IsCheckedChange;

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
            if (!this.isCheckedFlag)
                pevent.Graphics.DrawRectangle(borderPen, new Rectangle(0,0, Width - 1, Height - 1));
            else
                pevent.Graphics.DrawRectangle(checkedBorderPen, new Rectangle(checkedBorderWidth - 1,checkedBorderWidth - 1, 
                    Width - checkedBorderWidth*2, Height - checkedBorderWidth*2));

            drawText(pevent);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            var controlRect = new Rectangle(1,1, Width - 1, Height - 1);
            var mousePosition = this.PointToClient(CheckedButton.MousePosition);

            if (this.Focused || controlRect.Contains(mousePosition))
                pevent.Graphics.FillRectangle(mouseOverBrush, new Rectangle(0,0, Width - 1, Height - 1));
            else if (this.isCheckedFlag)
                pevent.Graphics.FillRectangle(checkedBackBrush, new Rectangle(0,0, Width - 1, Height - 1));
            else
                base.OnPaintBackground(pevent);
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);

            this.isCheckedFlag = !this.isCheckedFlag;
            Raise_IsCheckedChange();

            this.Invalidate();
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.Invalidate();
        }

        private void drawText(PaintEventArgs e)
        {
            var format = getControlTextFormat();
            var textRect = new Rectangle(Padding.Left, Padding.Top, 
                Width - Padding.Horizontal - 1, Height - Padding.Vertical - 1); 

            if (this.isCheckedFlag)
                e.Graphics.DrawString(this.Text, this.Font, checkedForeBrush, textRect, format);
            else
                e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(ForeColor), textRect, format);

            format.Dispose();
        }

        private StringFormat getControlTextFormat()
        {
            var format = new StringFormat();
            if ((ContentAlignment.TopCenter|ContentAlignment.TopLeft|ContentAlignment.TopRight).HasFlag(textAlign))
                format.LineAlignment = StringAlignment.Near;
            else if ((ContentAlignment.MiddleCenter|ContentAlignment.MiddleLeft|ContentAlignment.MiddleRight).HasFlag(textAlign))
                format.LineAlignment = StringAlignment.Center;
            else if ( (ContentAlignment.BottomCenter|ContentAlignment.BottomLeft|ContentAlignment.BottomRight).HasFlag(textAlign) )
                format.LineAlignment = StringAlignment.Far;

            if ((ContentAlignment.TopLeft|ContentAlignment.BottomLeft|ContentAlignment.MiddleLeft).HasFlag(textAlign))
                format.Alignment = StringAlignment.Near;
            else if ((ContentAlignment.MiddleCenter|ContentAlignment.BottomCenter|ContentAlignment.TopCenter).HasFlag(textAlign))
                format.Alignment = StringAlignment.Center;
            else if ( (ContentAlignment.TopRight|ContentAlignment.MiddleRight|ContentAlignment.BottomRight).HasFlag(textAlign) )
                format.Alignment = StringAlignment.Far;

            return format;
        }

        private void Raise_IsCheckedChange()
        {
            if (IsCheckedChange != null)
                IsCheckedChange(this, new EventArgs());
        }

        private bool isCheckedFlag = false;

        private Brush checkedBackBrush = SystemBrushes.Highlight;
        private Brush checkedForeBrush = SystemBrushes.ControlText;
        
        private Brush mouseOverBrush = SystemBrushes.ControlLight;
        private Brush mouseDownBrush = SystemBrushes.ControlLightLight;

        private Pen borderPen = SystemPens.ControlDark;
        private Pen checkedBorderPen = SystemPens.ControlDark;

        private int checkedBorderWidth = 0;
        
        private ContentAlignment textAlign = ContentAlignment.MiddleCenter;
    }
}