﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Diablo2ZyElRunewords.Controls
{
    public class DropedListView: Control
    {
        public DropedListView(): base()
        {
            itemList = new ItemList();
        }

        protected override void Dispose(bool disposing)
        {
            itemList.Dispose();
            base.Dispose(disposing);

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (this.Focused)
                //e.Graphics.DrawRectangle(SystemPens.ActiveBorder, new Rectangle(0, 0, Width - 1, Height - 1));
            TextBoxRenderer.DrawTextBox(e.Graphics, new Rectangle(0, 0, Width - 1, Height - 1), System.Windows.Forms.VisualStyles.TextBoxState.Hot);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            this.Invalidate();
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            Invalidate();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            this.Select();

            var desctopRect = SystemInformation.WorkingArea;

            var thisScreenLocation = this.PointToScreen(this.Location);

            if (thisScreenLocation.Y > desctopRect.Height / 2)
            {
                itemList.Size = new Size(this.Width, thisScreenLocation.Y);
                itemList.Location = new Point(thisScreenLocation.X, 0);
                itemList.Show();
            }
            else
            {
                itemList.Size = new Size(this.Width, desctopRect.Height - thisScreenLocation.Y);
                itemList.Location = new Point(thisScreenLocation.X, thisScreenLocation.Y + this.Height);
                itemList.Show();
            }
        }

        bool checkedItems;
        Size itemSize;
        private ToolTip toolTip1;
        private System.ComponentModel.IContainer components;

        ItemList itemList;

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            this.ResumeLayout(false);

        }

        
    }

    public enum PopupItemListElemView
    {
        Icons,
        Text,
        TextUnderIcons
    }

    public interface IPopupItemListElement
    {
        Image Image{get;}
        String Text{get;}
    }

    class ItemList: ContainerControl
    {
        //public ItemList(): base()
        //{
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //}

        //protected override void OnDeactivate(EventArgs e)
        //{
        //    base.OnDeactivate(e);

        //    this.Hide();
        //}

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.DrawRectangle(SystemPens.ControlLight, this.ClientRectangle);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams parameters = base.CreateParams;
                parameters.ClassStyle = 0x20000 | 0x1000| 8;
                //parameters.Style = 0x56cf0000;
                //parameters.ExStyle = 0x00050001;
                return parameters;
            }
        }

    }
}
