﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords
{
    public partial class TransparencyTextBox : Control
    {
        public TransparencyTextBox()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer| 
                ControlStyles.SupportsTransparentBackColor, true);

            this.DoubleBuffered = true;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);

            this.Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(ForeColor), this.ClientRectangle);
        }
    }
}
