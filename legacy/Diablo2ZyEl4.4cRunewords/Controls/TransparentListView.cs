﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords
{
    public partial class TransparentListView : ScrollableControl
    {
        public TransparentListView()
        {
            InitializeComponent();

            rows = new TransparentListViewRowsCollection();
            columns = new TransparentListViewColumnsCollection();

            headerForeColor = ForeColor;
            headerFont = new Font(Font.FontFamily, Font.Size *1.5F);
            headerBackColor = SystemColors.ButtonShadow;

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor| ControlStyles.Opaque, true);
        }

        public TransparentListViewRowsCollection Rows
        {
            get { return rows; }
        }

        public TransparentListViewColumnsCollection Columns
        {
            get { return columns; }
        }
        
        public Color HeaderForeColor
        {
            get { return headerForeColor; }
            set { headerForeColor = value; }
        }
        
        public Font HeaderFont
        {
            get { return headerFont; }
            set { headerFont = value; }
        }
        
        public Color HeaderBackColor
        {
            get { return headerBackColor; }
            set { headerBackColor = value; }
        }

        protected void OnDrawHeader(PaintEventArgs pe)
        {
            int headerHeight = 0, headerWidth = 0;
            Brush headerBackgroundBrush = new SolidBrush(headerBackColor);
            Brush headerTextBrush = new SolidBrush(headerForeColor);


            headerHeight = (int)Math.Floor(pe.Graphics.MeasureString("A", headerFont).Height);
            foreach(TransparentListViewColumn column in this.columns)
            {
                Rectangle columnHeaderRectangle  = new Rectangle(headerWidth, 0, column.Width, headerHeight);
                pe.Graphics.FillRectangle(headerBackgroundBrush, columnHeaderRectangle);
                pe.Graphics.DrawString(column.Text, headerFont, headerTextBrush, columnHeaderRectangle);

                headerWidth += column.Width;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            OnDrawHeader(pe);

            //int headerHeight = 0, headerWidth = 0;

            //headerHeight = (int)Math.Floor(pe.Graphics.MeasureString("", headerFont).Height);
            //foreach(TransparentListViewColumn column in this.columns)
            //{
            //    //pe.Graphics.FillRectangle();
            //    headerWidth += column.Width;
            //}
        }

        TransparentListViewRowsCollection rows;
        TransparentListViewColumnsCollection columns;

        Color headerForeColor;
        Font headerFont;
        Color headerBackColor;
    }

    public class TransparentListViewRowsCollection
    {
        
    }

    public class TransparentListViewRow: List<TransparentListViewCell>
    {
        public void Add(string Text)
        {
            var cell = new TransparentListViewCell(Text);

            this.Add(cell);
        }
    }

    public class TransparentListViewCell
    {
        public TransparentListViewCell()
        {}
        
        public TransparentListViewCell(string Text)
        {
            this.text = Text;
        }

        public string Text
        {
          get { return text; }
          set { text = value; }
        }

        string text;
    }

    public class TransparentListViewColumnsCollection: List<TransparentListViewColumn>
    {
        public void Add(string Text, int Width)
        {
            var column = new TransparentListViewColumn(Text, Width);

            this.Add(column);
        }

        public void Add(string Text)
        {
            var column = new TransparentListViewColumn(Text);

            this.Add(column);
        }
    }

    public class TransparentListViewColumn
    {
        public TransparentListViewColumn()
        {}

        public TransparentListViewColumn(string Text)
        {
            this.text = Text;

            this.width = MINWIDTH;
        }

        public TransparentListViewColumn(string Text, int Width)
        {
            this.text = Text;

            this.width = Width;
        }

        public string Text
        {
          get { return text; }
          set { text = value; }
        }

        public string Name
        {
          get { return name; }
          set { name = value; }
        }
        
        public int Width
        {
          get { return width; }
          set { width = value; }
        }

        static int MINWIDTH = 100;
        
        string name, text;
        int width;
    }
}
