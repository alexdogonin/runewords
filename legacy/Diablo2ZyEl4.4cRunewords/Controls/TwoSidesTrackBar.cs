﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords.Controls
{
    public partial class TwoSidesTrackBar : Control
    {
        #region public methods
        public TwoSidesTrackBar()
        {
            InitializeComponent();

            // пользовательская версия InitializeComponent
            initMembersByDefaultValues();
        }

        public void SetBorderValuesRange(int min, int max)
        {
            minValue = min;
            Raise_MinValueChanged(new EventArgs());
            maxValue = max;
            Raise_MaxValueChanged(new EventArgs());
        }

        public void SetValuesRange(int lessValue, int greateValue)
        {
            this.smallerValue = lessValue;
            Raise_LessValueChanged(new EventArgs());
            this.greaterValue = greateValue;
            Raise_GreaterValueChanged(new EventArgs());
        }
        #endregion

        #region public properties
        [DefaultValue(20)]
        public int MaxValue
        {
            get { return maxValue; }
            set { 
                maxValue = Math.Max(value, minValue + 1); 
                Raise_MaxValueChanged(new EventArgs());
                if (greaterValue > maxValue)
                {
                    greaterValue = maxValue;
                    Raise_GreaterValueChanged(new EventArgs());
                }
                if (smallerValue >= maxValue)
                {
                    smallerValue = maxValue - 1;
                    Raise_LessValueChanged(new EventArgs());
                }
            }
        }
        [DefaultValue(0)]
        public int MinValue
        {
            get { return minValue; }
            set { 
                minValue = Math.Min(value, maxValue - 1); 
                Raise_MinValueChanged(new EventArgs());
                if (smallerValue < minValue)
                {
                    smallerValue = minValue;
                    Raise_LessValueChanged(new EventArgs());
                }
                if (greaterValue <= minValue)
                {
                    greaterValue = minValue + 1;
                    Raise_GreaterValueChanged(new EventArgs());
                }
            }
        }
        [DefaultValue(1)]
        public int StepChanging
        {
            get { return stepChanging; }
            set { 
                stepChanging = value; 
                Raise_StepChangingChanged(new EventArgs());
            }
        }
        [DefaultValue(5)]
        public int SmallerValue
        {
            get { return smallerValue; }
            set { 
                smallerValue = Math.Min( Math.Max(minValue, value), greaterValue-1); 
                Raise_LessValueChanged(new EventArgs());
            }
        }
        [DefaultValue(15)]
        public int GreaterValue
        {
            get { return greaterValue; }
            set { 
                greaterValue = Math.Max(Math.Min(value, maxValue), smallerValue+1); 
                Raise_GreaterValueChanged(new EventArgs());
            }
        }
        [DefaultValue(false)]
        public bool HasBorder
        {
            get { return hasBorder; }
            set { hasBorder = value; }
        }
        [DefaultValue(System.Windows.Forms.Orientation.Horizontal)]
        public System.Windows.Forms.Orientation Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }
        [DefaultValue("SystemColors.Info")]
        public Color SelectedRangeColor
        {
            get {return selectedRangeColor;}
            set {
                selectedRangeColor = value;
                selectedRangeBrush = new SolidBrush(selectedRangeColor);
                this.Invalidate();
            }
        }
        [DefaultValue( typeof(Color), "SystemColors.ControlDark")]
        public Color OutOfSelectedRangeColor
        {
            get {return outOfSelectedRangeColor;}
            set {
                outOfSelectedRangeColor = value;
                outOfSelectedRangeBrush = new SolidBrush(outOfSelectedRangeColor);
                this.Invalidate();
            }
        }
        [DefaultValue("ControlLight")]
        public Color SelectedRandgeBorderColor
        {
            get { return selectedRandgeBorderColor; }
            set { 
                selectedRandgeBorderColor = value; 
                selectedRandgeBorderPen = new Pen(selectedRandgeBorderColor, SELECTED_RANGE_BORDER_WIDTH);
                this.Invalidate();
            }
        }
        [DefaultValue("Color [ControlLight]")]
        public Color BorderColor
        {
            get { return borderColor; }
            set { 
                borderColor = value; 
                borderPen = new Pen(borderColor);
                this.Invalidate();
            }
        }
        #endregion

        #region public events
        public event EventHandler MaxValueChanged;
        public event EventHandler MinValueChanged;
        public event EventHandler StepChangingChanged;
        public event EventHandler SmallerValueChanged;
        public event EventHandler GreaterValueChanged;
        #endregion

        #region protected methods
        protected override void OnPaint(PaintEventArgs pe)
        {
            var gr = pe.Graphics;
            
            gr.FillRectangle(outOfSelectedRangeBrush, 0, 0, smallerValBorderPos, Height);
            gr.FillRectangle(outOfSelectedRangeBrush, greaterValBorderPos, 0, Width - greaterValBorderPos, Height);
            gr.FillRectangle(selectedRangeBrush, smallerValBorderPos, 0, greaterValBorderPos - smallerValBorderPos, Height);
            
            gr.DrawLine(selectedRandgeBorderPen, smallerValBorderPos+1, 0, smallerValBorderPos+1, Height);
            gr.DrawLine(selectedRandgeBorderPen, greaterValBorderPos-1 ,0, greaterValBorderPos-1, Height);

            if (hasBorder)
                gr.DrawRectangle(borderPen, new Rectangle(0, 0, Width - 1, Height - 1));

            
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (isMouseOverBorder(e))
                this.Cursor = Cursors.SizeWE;
            else if (this.Cursor == Cursors.SizeWE && !isSmallerValueChanging && !isGreateValueChanging)
                this.Cursor = Cursors.Default;

            var diffVal = (int)((double)(e.X - startValueChangingPos) / this.Width * (this.maxValue - this.minValue));
            var diffValBySteps = diffVal - diffVal % stepChanging;
            

            if (isSmallerValueChanging)
            {
                SmallerValue = startValue +  diffValBySteps * stepChanging;
                this.Invalidate();
            }
            if (isGreateValueChanging)
            {
                GreaterValue = startValue + diffValBySteps * stepChanging;
                this.Invalidate();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (Math.Abs(smallerValBorderPos - e.X) <= 2)
            {
                isSmallerValueChanging = true;
                startValueChangingPos = e.X;
                startValue = smallerValue;
            }
            else if (Math.Abs(greaterValBorderPos - e.X) <= 2)
            {
                isGreateValueChanging = true;
                startValueChangingPos = e.X;
                startValue = greaterValue;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            
            float coef = (float)Width/maxValue;

            if (isGreateValueChanging)
            {
                isGreateValueChanging = false;
                this.Invalidate();
            }

            if (isSmallerValueChanging)
            {
                isSmallerValueChanging = false;
                this.Invalidate();
            }
        }

        protected void Raise_MaxValueChanged(EventArgs e)
        {
            if (MaxValueChanged != null)
                MaxValueChanged(this, e);
            this.Invalidate();
        }

        protected void Raise_MinValueChanged(EventArgs e)
        {
            if (MinValueChanged != null)
                MinValueChanged(this, e);
            this.Invalidate();
        }

        protected void Raise_StepChangingChanged(EventArgs e)
        {
            if (StepChangingChanged != null)
                StepChangingChanged(this, e);
        }

        protected void Raise_LessValueChanged(EventArgs e)
        {
            updateValueBordersPos();

            if (SmallerValueChanged != null)
                SmallerValueChanged(this, e);
            this.Invalidate();
        }

        protected void Raise_GreaterValueChanged(EventArgs e)
        {
            updateValueBordersPos();

            if (GreaterValueChanged != null)
                GreaterValueChanged(this, e);
            this.Invalidate();
        }

        override protected void OnResize(EventArgs e)
        {
            updateValueBordersPos();
 	        base.OnResize(e);
            
        }
        #endregion

        #region private methods
        private void initMembersByDefaultValues()
        {
            this.Size = new Size(100, 35);
            this.DoubleBuffered = true;

            this.MaxValue = 20;
            this.MinValue = 0;
            this.StepChanging = 1;
            this.SmallerValue = 5;
            this.GreaterValue = 15;
            this.HasBorder = true;
            this.Orientation = System.Windows.Forms.Orientation.Horizontal;

            selectedRandgeBorderColor = SystemColors.ControlLight;
            selectedRandgeBorderPen = new Pen(SystemPens.ControlLight.Color, SELECTED_RANGE_BORDER_WIDTH);;
            borderColor = SystemColors.ControlLight;
            borderPen = SystemPens.ControlLight;
            selectedRangeColor = SystemColors.Info;
            selectedRangeBrush = SystemBrushes.Info;
            outOfSelectedRangeColor = SystemColors.ControlDark;
            outOfSelectedRangeBrush = SystemBrushes.ControlDark;
        }

        private bool isMouseOverBorder(MouseEventArgs e)
        {
            return (Math.Abs(smallerValBorderPos - e.X) <= 2) || (Math.Abs(greaterValBorderPos - e.X) <= 2);
        }

        private void updateValueBordersPos()
        {
            greaterValBorderPos = (int)(((float)greaterValue/(float)maxValue) * (float)Width);
            smallerValBorderPos = (int)(((float)smallerValue/(float)maxValue) * (float)Width);
        }
        #endregion

        #region private members
        private const int BORDER_WIDTH = 1;
        private const int SELECTED_RANGE_BORDER_WIDTH = 2;

        private int maxValue;
        private int minValue;
        private int stepChanging;
        private int smallerValue;
        private int greaterValue;
        private bool hasBorder;

        private System.Windows.Forms.Orientation orientation;

        private Color selectedRangeColor;
        private Brush selectedRangeBrush;
        private Color outOfSelectedRangeColor;
        private Brush outOfSelectedRangeBrush;
        private Color selectedRandgeBorderColor;
        private Pen selectedRandgeBorderPen;
        private Color borderColor;
        private Pen borderPen;

        private int smallerValBorderPos;
        private int greaterValBorderPos;

        private bool isSmallerValueChanging;
        private bool isGreateValueChanging;
        private int startValueChangingPos;
        private int startValue;
        #endregion
    }

    class TwoSidesTrackBarRenderer
    {
        static public void Draw(PaintEventArgs pe)
        {
            //var gr = pe.Graphics;
            
            //gr.FillRectangle(outOfSelectedRangeBrush, 0, 0, smallerValBorderPos, Height);
            //gr.FillRectangle(outOfSelectedRangeBrush, greaterValBorderPos, 0, Width - greaterValBorderPos, Height);
            //gr.FillRectangle(selectedRangeBrush, smallerValBorderPos, 0, greaterValBorderPos - smallerValBorderPos, Height);
            //gr.DrawLine(selectedRandgeBorderPen, smallerValBorderPos, 0, smallerValBorderPos, Height);
            //gr.DrawLine(selectedRandgeBorderPen, greaterValBorderPos, 0, greaterValBorderPos, Height);
            //if (hasBorder)
            //    gr.DrawRectangle(borderPen, ClientRectangle);
        }
    }
    
}
