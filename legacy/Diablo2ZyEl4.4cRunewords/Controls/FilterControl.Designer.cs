﻿namespace Diablo2ZyElRunewords
{
    partial class RunewordFilterControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.runesPanel = new System.Windows.Forms.Panel();
            this.itemClassPanel = new System.Windows.Forms.Panel();
            this.personClassPanel = new System.Windows.Forms.Panel();
            this.levelPanel = new System.Windows.Forms.Panel();
            this.greaterLevelLabel = new System.Windows.Forms.Label();
            this.smallerLevelLabel = new System.Windows.Forms.Label();
            this.holeCountPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.runewordNamePanel = new System.Windows.Forms.Panel();
            this.runewordNameTextBox = new System.Windows.Forms.TextBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.runesButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.itemClassButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.personClassButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.levelButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.holeCountButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.runewordNameButton = new Diablo2ZyElRunewords.Controls.CheckedButton();
            this.runesControl = new Diablo2ZyElRunewords.Controls.TileComboBox();
            this.itemClassesControl = new Diablo2ZyElRunewords.Controls.TileComboBox();
            this.personClassesControl = new Diablo2ZyElRunewords.Controls.TileComboBox();
            this.runewordsLevelTrackbar = new Diablo2ZyElRunewords.Controls.TwoSidesTrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.runesPanel.SuspendLayout();
            this.itemClassPanel.SuspendLayout();
            this.personClassPanel.SuspendLayout();
            this.levelPanel.SuspendLayout();
            this.holeCountPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.runewordNamePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Size = new System.Drawing.Size(569, 582);
            this.splitContainer1.SplitterDistance = 234;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.splitContainer2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(5, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 282);
            this.panel1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.runesButton);
            this.splitContainer2.Panel1.Controls.Add(this.itemClassButton);
            this.splitContainer2.Panel1.Controls.Add(this.personClassButton);
            this.splitContainer2.Panel1.Controls.Add(this.levelButton);
            this.splitContainer2.Panel1.Controls.Add(this.holeCountButton);
            this.splitContainer2.Panel1.Controls.Add(this.runewordNameButton);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.runesPanel);
            this.splitContainer2.Panel2.Controls.Add(this.itemClassPanel);
            this.splitContainer2.Panel2.Controls.Add(this.personClassPanel);
            this.splitContainer2.Panel2.Controls.Add(this.levelPanel);
            this.splitContainer2.Panel2.Controls.Add(this.holeCountPanel);
            this.splitContainer2.Panel2.Controls.Add(this.runewordNamePanel);
            this.splitContainer2.Size = new System.Drawing.Size(547, 275);
            this.splitContainer2.SplitterDistance = 181;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 1;
            // 
            // runesPanel
            // 
            this.runesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.runesPanel.Controls.Add(this.runesControl);
            this.runesPanel.Location = new System.Drawing.Point(0, 226);
            this.runesPanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.runesPanel.Name = "runesPanel";
            this.runesPanel.Size = new System.Drawing.Size(357, 45);
            this.runesPanel.TabIndex = 7;
            // 
            // itemClassPanel
            // 
            this.itemClassPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.itemClassPanel.Controls.Add(this.itemClassesControl);
            this.itemClassPanel.Location = new System.Drawing.Point(0, 180);
            this.itemClassPanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.itemClassPanel.Name = "itemClassPanel";
            this.itemClassPanel.Size = new System.Drawing.Size(357, 45);
            this.itemClassPanel.TabIndex = 5;
            this.itemClassPanel.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // personClassPanel
            // 
            this.personClassPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.personClassPanel.Controls.Add(this.personClassesControl);
            this.personClassPanel.Location = new System.Drawing.Point(0, 135);
            this.personClassPanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.personClassPanel.Name = "personClassPanel";
            this.personClassPanel.Size = new System.Drawing.Size(357, 45);
            this.personClassPanel.TabIndex = 4;
            this.personClassPanel.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // levelPanel
            // 
            this.levelPanel.Controls.Add(this.runewordsLevelTrackbar);
            this.levelPanel.Controls.Add(this.greaterLevelLabel);
            this.levelPanel.Controls.Add(this.smallerLevelLabel);
            this.levelPanel.Location = new System.Drawing.Point(0, 90);
            this.levelPanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.levelPanel.Name = "levelPanel";
            this.levelPanel.Size = new System.Drawing.Size(357, 45);
            this.levelPanel.TabIndex = 3;
            this.levelPanel.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // greaterLevelLabel
            // 
            this.greaterLevelLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.greaterLevelLabel.Location = new System.Drawing.Point(322, 0);
            this.greaterLevelLabel.Name = "greaterLevelLabel";
            this.greaterLevelLabel.Size = new System.Drawing.Size(35, 45);
            this.greaterLevelLabel.TabIndex = 3;
            this.greaterLevelLabel.Text = "255";
            this.greaterLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // smallerLevelLabel
            // 
            this.smallerLevelLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.smallerLevelLabel.Location = new System.Drawing.Point(0, 0);
            this.smallerLevelLabel.Name = "smallerLevelLabel";
            this.smallerLevelLabel.Size = new System.Drawing.Size(35, 45);
            this.smallerLevelLabel.TabIndex = 2;
            this.smallerLevelLabel.Text = "0";
            this.smallerLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // holeCountPanel
            // 
            this.holeCountPanel.Controls.Add(this.flowLayoutPanel1);
            this.holeCountPanel.Location = new System.Drawing.Point(0, 45);
            this.holeCountPanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.holeCountPanel.Name = "holeCountPanel";
            this.holeCountPanel.Size = new System.Drawing.Size(357, 45);
            this.holeCountPanel.TabIndex = 2;
            this.holeCountPanel.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.checkBox1);
            this.flowLayoutPanel1.Controls.Add(this.checkBox2);
            this.flowLayoutPanel1.Controls.Add(this.checkBox3);
            this.flowLayoutPanel1.Controls.Add(this.checkBox4);
            this.flowLayoutPanel1.Controls.Add(this.checkBox5);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 10);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(324, 29);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(3, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(32, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "2";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(41, 3);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(32, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "3";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(79, 3);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(32, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "4";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(117, 3);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(32, 17);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "5";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(155, 3);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(32, 17);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "6";
            this.checkBox5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // runewordNamePanel
            // 
            this.runewordNamePanel.Controls.Add(this.runewordNameTextBox);
            this.runewordNamePanel.Location = new System.Drawing.Point(0, 0);
            this.runewordNamePanel.Margin = new System.Windows.Forms.Padding(10);
            this.runewordNamePanel.MinimumSize = new System.Drawing.Size(0, 45);
            this.runewordNamePanel.Name = "runewordNamePanel";
            this.runewordNamePanel.Size = new System.Drawing.Size(357, 45);
            this.runewordNamePanel.TabIndex = 1;
            this.runewordNamePanel.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // runewordNameTextBox
            // 
            this.runewordNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.runewordNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.runewordNameTextBox.Location = new System.Drawing.Point(3, 13);
            this.runewordNameTextBox.Name = "runewordNameTextBox";
            this.runewordNameTextBox.Size = new System.Drawing.Size(351, 20);
            this.runewordNameTextBox.TabIndex = 0;
            // 
            // acceptButton
            // 
            this.acceptButton.BackgroundImage = global::Diablo2ZyElRunewords.Properties.Resources.Accept;
            this.acceptButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.acceptButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acceptButton.Location = new System.Drawing.Point(498, 0);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(24, 24);
            this.acceptButton.TabIndex = 2;
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.BackgroundImage = global::Diablo2ZyElRunewords.Properties.Resources.Clear;
            this.clearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.clearButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.clearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearButton.Location = new System.Drawing.Point(522, 0);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(24, 24);
            this.clearButton.TabIndex = 1;
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // runesButton
            // 
            this.runesButton.BorderColor = System.Drawing.SystemColors.Control;
            this.runesButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.runesButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.runesButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.runesButton.CheckedBorderWidth = 2;
            this.runesButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.runesButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.runesButton.IsChecked = true;
            this.runesButton.Location = new System.Drawing.Point(0, 225);
            this.runesButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.runesButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.runesButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.runesButton.Name = "runesButton";
            this.runesButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.runesButton.Size = new System.Drawing.Size(181, 45);
            this.runesButton.TabIndex = 5;
            this.runesButton.Text = "Руны";
            this.runesButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.runesButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // itemClassButton
            // 
            this.itemClassButton.BorderColor = System.Drawing.SystemColors.Control;
            this.itemClassButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.itemClassButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.itemClassButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.itemClassButton.CheckedBorderWidth = 2;
            this.itemClassButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.itemClassButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.itemClassButton.IsChecked = true;
            this.itemClassButton.Location = new System.Drawing.Point(0, 180);
            this.itemClassButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.itemClassButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.itemClassButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.itemClassButton.Name = "itemClassButton";
            this.itemClassButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.itemClassButton.Size = new System.Drawing.Size(181, 45);
            this.itemClassButton.TabIndex = 4;
            this.itemClassButton.Text = "Класс вещи";
            this.itemClassButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.itemClassButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // personClassButton
            // 
            this.personClassButton.BorderColor = System.Drawing.SystemColors.Control;
            this.personClassButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.personClassButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.personClassButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.personClassButton.CheckedBorderWidth = 2;
            this.personClassButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.personClassButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.personClassButton.IsChecked = true;
            this.personClassButton.Location = new System.Drawing.Point(0, 135);
            this.personClassButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.personClassButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.personClassButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.personClassButton.Name = "personClassButton";
            this.personClassButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.personClassButton.Size = new System.Drawing.Size(181, 45);
            this.personClassButton.TabIndex = 3;
            this.personClassButton.Text = "Класс героя";
            this.personClassButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.personClassButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // levelButton
            // 
            this.levelButton.BorderColor = System.Drawing.SystemColors.Control;
            this.levelButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.levelButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.levelButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.levelButton.CheckedBorderWidth = 2;
            this.levelButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.levelButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.levelButton.IsChecked = true;
            this.levelButton.Location = new System.Drawing.Point(0, 90);
            this.levelButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.levelButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.levelButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.levelButton.Name = "levelButton";
            this.levelButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.levelButton.Size = new System.Drawing.Size(181, 45);
            this.levelButton.TabIndex = 2;
            this.levelButton.Text = "Уровень";
            this.levelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.levelButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // holeCountButton
            // 
            this.holeCountButton.BorderColor = System.Drawing.SystemColors.Control;
            this.holeCountButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.holeCountButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.holeCountButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.holeCountButton.CheckedBorderWidth = 2;
            this.holeCountButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.holeCountButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.holeCountButton.IsChecked = true;
            this.holeCountButton.Location = new System.Drawing.Point(0, 45);
            this.holeCountButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.holeCountButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.holeCountButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.holeCountButton.Name = "holeCountButton";
            this.holeCountButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.holeCountButton.Size = new System.Drawing.Size(181, 45);
            this.holeCountButton.TabIndex = 1;
            this.holeCountButton.Text = "Число сокетов";
            this.holeCountButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.holeCountButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // runewordNameButton
            // 
            this.runewordNameButton.BorderColor = System.Drawing.SystemColors.Control;
            this.runewordNameButton.ChechedBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.runewordNameButton.ChechedFontColor = System.Drawing.SystemColors.ControlText;
            this.runewordNameButton.CheckedBackColor = System.Drawing.SystemColors.Control;
            this.runewordNameButton.CheckedBorderWidth = 2;
            this.runewordNameButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.runewordNameButton.ForeColor = System.Drawing.SystemColors.GrayText;
            this.runewordNameButton.IsChecked = true;
            this.runewordNameButton.Location = new System.Drawing.Point(0, 0);
            this.runewordNameButton.MinimumSize = new System.Drawing.Size(0, 45);
            this.runewordNameButton.MouseDownColor = System.Drawing.SystemColors.ControlLightLight;
            this.runewordNameButton.MouseOverColor = System.Drawing.SystemColors.ControlLight;
            this.runewordNameButton.Name = "runewordNameButton";
            this.runewordNameButton.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.runewordNameButton.Size = new System.Drawing.Size(181, 45);
            this.runewordNameButton.TabIndex = 0;
            this.runewordNameButton.Text = "Название";
            this.runewordNameButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.runewordNameButton.SizeChanged += new System.EventHandler(this.filterFild_SizeChanged);
            // 
            // runesControl
            // 
            this.runesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.runesControl.IsCanMultiSelect = true;
            this.runesControl.Location = new System.Drawing.Point(0, 0);
            this.runesControl.Name = "runesControl";
            this.runesControl.Size = new System.Drawing.Size(357, 45);
            this.runesControl.TabIndex = 1;
            this.runesControl.Text = "tileComboBox3";
            this.runesControl.View = Diablo2ZyElRunewords.Controls.ContentView.List;
            // 
            // itemClassesControl
            // 
            this.itemClassesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemClassesControl.IsCanMultiSelect = true;
            this.itemClassesControl.Location = new System.Drawing.Point(0, 0);
            this.itemClassesControl.Name = "itemClassesControl";
            this.itemClassesControl.Size = new System.Drawing.Size(357, 45);
            this.itemClassesControl.TabIndex = 1;
            this.itemClassesControl.Text = "tileComboBox2";
            this.itemClassesControl.View = Diablo2ZyElRunewords.Controls.ContentView.List;
            // 
            // personClassesControl
            // 
            this.personClassesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.personClassesControl.IsCanMultiSelect = true;
            this.personClassesControl.Location = new System.Drawing.Point(0, 0);
            this.personClassesControl.Name = "personClassesControl";
            this.personClassesControl.Size = new System.Drawing.Size(357, 45);
            this.personClassesControl.TabIndex = 0;
            this.personClassesControl.Text = "tileComboBox1";
            this.personClassesControl.View = Diablo2ZyElRunewords.Controls.ContentView.TextOnly;
            // 
            // runewordsLevelTrackbar
            // 
            this.runewordsLevelTrackbar.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.runewordsLevelTrackbar.MaxValue = 255;
            this.runewordsLevelTrackbar.GreaterValue = 255;
            this.runewordsLevelTrackbar.HasBorder = true;
            this.runewordsLevelTrackbar.Location = new System.Drawing.Point(38, 9);
            this.runewordsLevelTrackbar.Margin = new System.Windows.Forms.Padding(0);
            this.runewordsLevelTrackbar.Name = "runewordsLevelTrackbar";
            this.runewordsLevelTrackbar.OutOfSelectedRangeColor = System.Drawing.SystemColors.ControlLight;
            this.runewordsLevelTrackbar.SelectedRandgeBorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.runewordsLevelTrackbar.SelectedRangeColor = System.Drawing.SystemColors.ControlLightLight;
            this.runewordsLevelTrackbar.Size = new System.Drawing.Size(281, 25);
            this.runewordsLevelTrackbar.SmallerValue = 0;
            this.runewordsLevelTrackbar.TabIndex = 4;
            this.runewordsLevelTrackbar.Text = "runewordsLevelTrackbar";
            this.runewordsLevelTrackbar.SmallerValueChanged += new System.EventHandler(this.levelTwoSidesTrackBar_SmallerValueChanged);
            this.runewordsLevelTrackbar.GreaterValueChanged += new System.EventHandler(this.levelTwoSidesTrackBar_GreaterValueChanged);
            // 
            // RunewordFilterControl
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.clearButton);
            this.Name = "RunewordFilterControl";
            this.Padding = new System.Windows.Forms.Padding(5, 26, 5, 5);
            this.Size = new System.Drawing.Size(559, 313);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.runesPanel.ResumeLayout(false);
            this.itemClassPanel.ResumeLayout(false);
            this.personClassPanel.ResumeLayout(false);
            this.levelPanel.ResumeLayout(false);
            this.holeCountPanel.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.runewordNamePanel.ResumeLayout(false);
            this.runewordNamePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Diablo2ZyElRunewords.Controls.CheckedButton runesButton;
        private Diablo2ZyElRunewords.Controls.CheckedButton itemClassButton;
        private Diablo2ZyElRunewords.Controls.CheckedButton personClassButton;
        private Diablo2ZyElRunewords.Controls.CheckedButton levelButton;
        private Diablo2ZyElRunewords.Controls.CheckedButton holeCountButton;
        private Diablo2ZyElRunewords.Controls.CheckedButton runewordNameButton;
        private System.Windows.Forms.Panel runewordNamePanel;
        private System.Windows.Forms.Panel itemClassPanel;
        private System.Windows.Forms.Panel personClassPanel;
        private System.Windows.Forms.Panel levelPanel;
        private System.Windows.Forms.TextBox runewordNameTextBox;
        private System.Windows.Forms.Panel holeCountPanel;
        private System.Windows.Forms.Label greaterLevelLabel;
        private System.Windows.Forms.Label smallerLevelLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private Controls.TwoSidesTrackBar runewordsLevelTrackbar;
        private System.Windows.Forms.Panel runesPanel;
        private Controls.TileComboBox runesControl;
        private Controls.TileComboBox itemClassesControl;
        private Controls.TileComboBox personClassesControl;
    }
}