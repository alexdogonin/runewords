﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords
{
    public partial class ComboCheckBox2 : ListControl
    {
        System.Windows.Forms.VisualStyles.TextBoxState currentTextState;
        public ComboCheckBox2()
        {
            InitializeComponent();

            currentTextState = System.Windows.Forms.VisualStyles.TextBoxState.Disabled;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void SetItemsCore(System.Collections.IList items)
        {
        
        }

        protected override void RefreshItem(int index)
        {
        
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            currentTextState = System.Windows.Forms.VisualStyles.TextBoxState.Hot;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            currentTextState = System.Windows.Forms.VisualStyles.TextBoxState.Normal;
            this.Invalidate();
        }

        public override int SelectedIndex 
        {
            get{return 0;}
            set{}
        }
    }
}
