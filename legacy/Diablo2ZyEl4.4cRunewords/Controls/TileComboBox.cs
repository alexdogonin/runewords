﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Diablo2ZyElRunewords.Controls
{
     /*
       Компонент представляет контейнер элементов списка, которые отображаются в выпадающем окне.
       Возможные варианты списка:
       1) Вертикальный список с вертикальной прокруткой
       2) Плитка слева направо с вертикальной и горизонтальной прокруткой
       Варианты отображения элементов списка:
       1) отображается только текст
     * 2) текст и иконка (с возможностью выбора положения текста относительно иконки)
     * 3) только иконка
     * Компонент может работать как ComboBox (после нажатия на элемент выпадающее окно закрывается),
     * так и в режиме CheckBox.
     * 
     */

    public partial class TileComboBox : Control
    {
        public TileComboBox()
        {
            InitializeComponent();

            popupControlsContainer = new PopupControlContainer();
            popupFrame = new PopupControl(popupControlsContainer);
            contentPanel = new FlowLayoutPanel();
            
            popupControlsContainer.Controls.Add(contentPanel);
            popupControlsContainer.Padding = new System.Windows.Forms.Padding(1, 1, 2, 10);
            popupControlsContainer.BackColor = this.BackColor;
            popupFrame.Resizable = true;

            contentPanel.Font      = this.Font;
            contentPanel.ForeColor = this.ForeColor;
            contentPanel.Dock      = DockStyle.Fill;
            contentPanel.AutoScroll = true;

            items = new TileComboboxItemsCollection();
            items.ItemAdded += new TileCmbItemsCollectionAddEventHandler(items_ItemAdded);
            items.ItemRemoved += new TileCmbItemsCollectionRemoveEventHandler(items_ItemRemoved);
            items.ItemsClear += new EventHandler(items_ItemsClear);

            selectedItems = new TileComboboxItemsCollection();
            selectedItems.ItemAdded += new TileCmbItemsCollectionAddEventHandler(selectedItems_ItemAdded);
            selectedItems.ItemRemoved += new TileCmbItemsCollectionRemoveEventHandler(selectedItems_ItemRemoved);
            selectedItems.ItemsClear += new EventHandler(selectedItems_ItemsClear);

            view = ContentView.ImageToLeftOfText;
            isCanMultiSelect = true;

        }

        public TileComboboxItemsCollection Items
        {
            get { return items; }
        }

        public TileComboboxItemsCollection SelectedItems
        {
#warning при такой реализации можно добавить элемент, которого нет в списке элементов items
            get { return selectedItems; }
        }

        public ContentView View
        {
            get { return view; }
            set { view = value; updateView();}
        }

        public bool IsCanMultiSelect
        {
            get { return isCanMultiSelect; }
            set { if (isCanMultiSelect != value) selectedItems.Clear();
                isCanMultiSelect = value; }
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            contentPanel.ForeColor = this.ForeColor;
        }

        protected override void OnFontChanged(EventArgs e)
        {
            contentPanel.Font = this.Font;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);   

            pe.Graphics.DrawRectangle(SystemPens.ControlLight, 0, 0, this.Width - 1, this.Height - 1);

            drawText(pe);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (isFirstPopupShow)
                {
                    popupFrame.Width = this.Width;
                    isFirstPopupShow = false;
                }
                popupFrame.Show(this);
            }   
        }

        private void drawText(PaintEventArgs pe)
        {
            var strBuilder = new StringBuilder();
            const string DELIM = ", ";
            bool firstIteration = true;
            var format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Near;
            format.FormatFlags = StringFormatFlags.NoWrap;
            format.Trimming = StringTrimming.EllipsisCharacter;

            foreach(TileComboBoxItem item in selectedItems)
            {
                if (!firstIteration) strBuilder.Append(DELIM);
                else firstIteration = false;
                strBuilder.Append(item.Text);
            }
            pe.Graphics.DrawString(strBuilder.ToString(), this.Font, new SolidBrush(this.ForeColor), (RectangleF)this.ClientRectangle, format);
        }

        private void updateView()
        {
            contentPanel.SuspendLayout();
            foreach(TileComboboxItemControl control in contentPanel.Controls)
                control.View = this.view;
            contentPanel.ResumeLayout();
        }

        private void items_ItemsClear(object sender, EventArgs e)
        {
            contentPanel.Controls.Clear();
            selectedItems.Clear();
        }

        private void items_ItemRemoved(object sender, TileComboboxItemsCollectionEventArgs args)
        {
            // TODO: алгоритм неоптимальный
            foreach(TileComboboxItemControl control in contentPanel.Controls)
                if (control.TileComboBoxItem.Equals(args.Item)) contentPanel.Controls.Remove(control);
            selectedItems.Remove(args.Item);
        }

        private void items_ItemAdded(object sender, TileComboboxItemsCollectionEventArgs args)
        {
            var control = new TileComboboxItemControl(args.Item);
            control.View = this.view;
            contentPanel.Controls.Add(control);
            control.Click += new EventHandler(contentPanelControlClick);
        }

        private void contentPanelControlClick(object sender, EventArgs e)
        {
            var selectedItemControl = (TileComboboxItemControl)sender;
            if (isCanMultiSelect) 
                if (selectedItems.Contains(selectedItemControl.TileComboBoxItem))
                    selectedItems.Remove(selectedItemControl.TileComboBoxItem);
                else
                    selectedItems.Add(selectedItemControl.TileComboBoxItem);
            else
            {
                selectedItems.Clear();
                selectedItems.Add(selectedItemControl.TileComboBoxItem);
            }

            Invalidate();
        }

       private  void selectedItems_ItemsClear(object sender, EventArgs e)
        {
            foreach(Control control in contentPanel.Controls)
                ((TileComboboxItemControl)control).Checked = false;

            this.Invalidate();
        }

        private void selectedItems_ItemRemoved(object sender, TileComboboxItemsCollectionEventArgs args)
        {
            foreach(Control control in contentPanel.Controls)
                if (((TileComboboxItemControl)control).TileComboBoxItem == args.Item)
                    ((TileComboboxItemControl)control).Checked = false;
            
            this.Invalidate();
        }

        private void selectedItems_ItemAdded(object sender, TileComboboxItemsCollectionEventArgs args)
        {
            foreach(Control control in contentPanel.Controls)
                if (((TileComboboxItemControl)control).TileComboBoxItem == args.Item)
                    ((TileComboboxItemControl)control).Checked = true;

            this.Invalidate();
        }

        private FlowLayoutPanel contentPanel;
        private PopupControl popupFrame;
        private PopupControlContainer popupControlsContainer;
        private TileComboboxItemsCollection items;
        private TileComboboxItemsCollection selectedItems;

        private ContentView view;
        private bool isCanMultiSelect;
        private bool isFirstPopupShow = true;
    }

    public class TileComboBoxItem
    {
        public TileComboBoxItem(string text, Image img):this(text)
        {
            this.image = img;
        }

        public TileComboBoxItem(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get { return text; }
            set { 
                text = value; 
                if (ItemChanged != null) ItemChanged(this, new EventArgs());
            }
        }

        public Image Image
        {
            get { return image; }
            set { 
                image = value; 
                if (ItemChanged != null) ItemChanged(this, new EventArgs());
            }
        }

        public Object Tag
        {
            get{return tag;}
            set{tag = value;}
        }

        public event EventHandler ItemChanged;

        private string text;
        private Image image;
        private Object tag;
    }

    public class TileComboboxItemsCollection: ICollection<TileComboBoxItem>
    {
        #region public methods
        public TileComboboxItemsCollection()
        {
            this.items = new List<TileComboBoxItem>();
        }

        public TileComboboxItemsCollection(IEnumerable<TileComboBoxItem> items)
        {
            this.items = new List<TileComboBoxItem>(items);
        }
        #endregion

        #region interface implementation

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator<TileComboBoxItem> IEnumerable<TileComboBoxItem>.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public int Count 
        { 
            get{return items.Count;} 
        }

        public bool IsReadOnly 
        { 
            get{return false;} 
        }

        public void Add(TileComboBoxItem item)
        {
            items.Add(item);
            RaiseItemAdded(item);
        }
        
        public bool Remove(TileComboBoxItem item)
        {
            var result = items.Remove(item);
            if (result) RaiseItemRemoved(item);
            return result;
        }

        public void Clear()
        {
            items.Clear();
            RaiseItemsClear();
        }

        public bool Contains(TileComboBoxItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(TileComboBoxItem[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        #endregion

        #region protected methods
        protected void RaiseItemAdded(TileComboBoxItem item)
        {
            if (ItemAdded != null) ItemAdded(this, new TileComboboxItemsCollectionEventArgs(item));
        }

        protected void RaiseItemRemoved(TileComboBoxItem item)
        {
            if (ItemRemoved != null) ItemRemoved(this, new TileComboboxItemsCollectionEventArgs(item));
        }

        protected void RaiseItemsClear()
        {
            if (ItemsClear != null) ItemsClear(this, new EventArgs());
        }
        #endregion

        #region public events
        public event TileCmbItemsCollectionAddEventHandler ItemAdded;
        public event TileCmbItemsCollectionRemoveEventHandler ItemRemoved;
        public event EventHandler ItemsClear;
        #endregion

        #region private members
        List<TileComboBoxItem> items;
        #endregion
    }

    public delegate void TileCmbItemsCollectionAddEventHandler(object sender, TileComboboxItemsCollectionEventArgs args);
    public delegate void TileCmbItemsCollectionRemoveEventHandler(object sender, TileComboboxItemsCollectionEventArgs args);

    public class TileComboboxItemsCollectionEventArgs: EventArgs
    {
        public TileComboboxItemsCollectionEventArgs(TileComboBoxItem item): base()
        {
            this.item = item;
        }

        public TileComboBoxItem Item
        {
          get { return item; }
        }

        private TileComboBoxItem item;
    }

    public enum ContentView
    {
        TextUnderImage,
        ImageToLeftOfText,
        TextToLeftOfImage,
        ImageUnderText,
        TextOnly,
        List
    }

    class TileComboboxItemControl: Control
    {
        #region public methods
        public TileComboboxItemControl(TileComboBoxItem item)
        {
            this.Padding = new System.Windows.Forms.Padding(3);
            tileComboBoxItem = item;
            item.ItemChanged += new EventHandler(item_ItemChanged);
            contentView = ContentView.ImageToLeftOfText;
            updateSize();
        }
        #endregion

        #region public properties
        public bool Checked
        {
            get{return isChecked;}
            set{isChecked = value; Invalidate();}
        }

        public TileComboBoxItem TileComboBoxItem
        {
            get { return tileComboBoxItem; }
        }

        public ContentView View
        {
            get { return contentView; }
            set { contentView = value; updateSize(); }
        }
        #endregion

        #region protected methods
        protected override void OnPaint(PaintEventArgs e)
        {
            var format = new StringFormat();
            format.FormatFlags = StringFormatFlags.NoWrap;
            format.Trimming = StringTrimming.EllipsisCharacter;
            base.OnPaint(e);

            e.Graphics.DrawString(tileComboBoxItem.Text, this.Font, new SolidBrush(this.ForeColor), textRect, format);

            e.Graphics.DrawImage((tileComboBoxItem.Image != null ? tileComboBoxItem.Image : Diablo2ZyElRunewords.Properties.Resources.unknown),
                imageRect);

            e.Graphics.DrawRectangle(SystemPens.ControlLight, 0, 0, this.Width - 1, this.Height - 1);

            if (isChecked)
                drawCheckedBorder(e);
        }
        #endregion

        #region private methods
        private void drawCheckedBorder(PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(checkedBorderPen, 1, 1, this.Width - 2, this.Height - 2);
        }

        private void item_ItemChanged(object sender, EventArgs e)
        {
            updateSize();
            this.Invalidate();
        }

        private void updateSize()
        {
            Size textSize = TextRenderer.MeasureText(tileComboBoxItem.Text, this.Font);
            // TODO: нужно сделать по нормальному. сейчас текст занимает больше места, чем предполагает рендерер
            textSize = new Size((int)(textSize.Width * 1.5), (int)(textSize.Height * 1.5));
            switch(this.contentView)
            {
                case ContentView.ImageToLeftOfText:
                    {
                        this.Size = this.Padding.Size + new Size(textSize.Width + imageSize.Width, Math.Max(textSize.Height, imageSize.Height));
                        imageRect = new Rectangle(this.Padding.Left, this.Padding.Top, imageSize.Width, imageSize.Height);
                        textRect = new Rectangle(imageRect.Right, (this.Height - textSize.Height)/2, textSize.Width, textSize.Height);
                    break;
                    }
                case ContentView.TextToLeftOfImage:
                    {
                        this.Size = this.Padding.Size + new Size(textSize.Width + imageSize.Width, Math.Max(textSize.Height, imageSize.Height));
                        textRect = new Rectangle(this.Padding.Left, this.Padding.Top, textSize.Width, textSize.Height);
                        imageRect = new Rectangle(textRect.Right, textRect.Top, imageSize.Width, imageSize.Height);
                    break;
                    }
                case ContentView.ImageUnderText:
                    {
                        this.Size = this.Padding.Size + new Size(Math.Max(textSize.Width, imageSize.Width), textSize.Height + imageSize.Height);
                        textRect = new Rectangle(this.Padding.Left, this.Padding.Top, textSize.Width, textSize.Height);
                        imageRect = new Rectangle((this.Width - imageSize.Width)/2, textRect.Bottom, imageSize.Width, imageSize.Height);
                    break;
                    }
                case ContentView.TextUnderImage:
                    {
                        this.Size = this.Padding.Size + new Size(Math.Max(textSize.Width, imageSize.Width), textSize.Height + imageSize.Height);
                        imageRect = new Rectangle((this.Width - imageSize.Width)/2, this.Padding.Top, imageSize.Width, imageSize.Height);
                        textRect = new Rectangle(Padding.Left, imageRect.Bottom, textSize.Width, textSize.Height);
                    break;
                    }
                case ContentView.TextOnly:
                    {
                        this.Size = this.Padding.Size + new Size(textSize.Width, Math.Max(textSize.Height, imageSize.Height));
                        imageRect = new Rectangle();
                        textRect = new Rectangle(this.Padding.Left, (this.Height - textSize.Height)/2, textSize.Width, textSize.Height);
                    break;
                    }
                case ContentView.List:
                    {
                        this.Size = this.Padding.Size + new Size(textSize.Width, textSize.Height);
                        imageRect = new Rectangle();
                        textRect = new Rectangle(this.Padding.Left, (this.Height - textSize.Height)/2, textSize.Width, textSize.Height);
                    break;
                    }
            }
        }
        #endregion

        #region private members
        private TileComboBoxItem tileComboBoxItem;
        private ContentView contentView;
        private readonly Size imageSize = new Size(32,32);
        private readonly Pen checkedBorderPen = new Pen(SystemColors.ActiveCaption, 2);
        private Rectangle imageRect;
        private Rectangle textRect;
        private bool isChecked;
        #endregion
    }
}
