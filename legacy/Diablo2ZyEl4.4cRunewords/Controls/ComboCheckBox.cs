﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Diablo2ZyElRunewords
{
    public partial class ComboCheckBox : TextBox
    {
        #region Constructors
        public ComboCheckBox()
            : base()
        {
            InitializeComponent();

            checkedDropDownList = new ListView();
            items = new ComboCheckBoxItemsCollection(this);
            isCheckedItemProcessed = false;
            isShownOnEnter         = true;
            isTextWasSelected      = false;
            isIgnoreLostFocusEvent = false;
            selectedTextOnMouseDown = "";

            checkedDropDownList.Visible = false;
            checkedDropDownList.MultiSelect = false;
            checkedDropDownList.CheckBoxes = true;
            checkedDropDownList.Width = MIN_WIDTH;
            checkedDropDownList.Height = MIN_HEIGHT;
            checkedDropDownList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            checkedDropDownList.LostFocus      += checkedDropDownList_LostFocus;
            //checkedDropDownList.VisibleChanged += checkedDropDownList_VisibleChanged;
            checkedDropDownList.KeyPress       += checkedDropDownList_KeyPress;
            checkedDropDownList.ItemChecked    += checkedDropDownList_ItemChecked;

            this.Enter       += ComboCheckBox_Enter;
            this.Click       += ComboCheckBox_Click;
            this.KeyPress    += ComboCheckBox_KeyPress;
            this.ItemChanged += ComboCheckBox_CheckedItem;
            this.LostFocus   += ComboCheckBox_LostFocus;
            this.MouseDown   += ComboCheckBox_MouseDown;
            this.MouseUp     += ComboCheckBox_MouseUp;
            this.Move        += ComboCheckBox_Move;
        }
        #endregion

        #region Public Methods
        public ComboCheckBoxItemsCollection GetCheckedItems()
        {
            ComboCheckBoxItemsCollection checkedItems = new ComboCheckBoxItemsCollection();
            foreach (ComboCheckBoxItem item in this.items)
                if (item.Checked)
                    checkedItems.Add(item);
            return checkedItems;
        }  
      
        public void OnChangedItems(ComboCheckBoxItemsChangedEventArg arg)
        {
            if (ItemChanged != null)
                ItemChanged(this, arg);
        }

        public ComboCheckBoxItemsCollection Items
        {
            get {
                return items;
            }
        }

        public View View
        {
            get { return checkedDropDownList.View; }
            set { checkedDropDownList.View = value; }
        }

        public ImageList Images
        {
            get { return checkedDropDownList.LargeImageList; }
            set { 
                checkedDropDownList.LargeImageList = value;
                checkedDropDownList.SmallImageList = value;
            }
        }

        public SortOrder Sorting
        {
            get { return checkedDropDownList.Sorting; }
            set { checkedDropDownList.Sorting = value; }
        }
        #endregion

        #region Protected Methods

        #endregion

        #region Private Event 

        private void ComboCheckBox_MouseDown(object sender, MouseEventArgs e)
        {
            selectedTextOnMouseDown = this.SelectedText;
            isTextWasSelected = false;
        }

        private void ComboCheckBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (selectedTextOnMouseDown != this.SelectedText)
                isTextWasSelected = true;

            if(checkedDropDownList.Visible && !isShownOnEnter && !isTextWasSelected)
                dropDownedListHide();
            else if(!checkedDropDownList.Visible && !isTextWasSelected)
            {
                dropedDownListShow();
            }
            
            isShownOnEnter = false;
        }

        private void ComboCheckBox_LostFocus(object sender, EventArgs e)
        {
            if (!isIgnoreLostFocusEvent && !checkedDropDownList.Focused)
                dropDownedListHide();
        }

        private void ComboCheckBox_CheckedItem(object sender, ComboCheckBoxItemsChangedEventArg e)
        {
            if (e.ChangesType == ComboCheckBoxChangesType.Add)
            {
                ListViewItem newItem = new ListViewItem(e.Item.FullText.Length > 0 ? e.Item.FullText : e.Item.ShortText, e.Item.ImageIndex);
                newItem.Tag = e.Item;
                newItem.Name = e.Item.ShortText;
                checkedDropDownList.Items.Add(newItem);
            }
            if (e.ChangesType == ComboCheckBoxChangesType.Delete)
            {
                ListViewItem[] foundedItems = checkedDropDownList.Items.Find(e.Item.ShortText, false);
                if (foundedItems.Length > 0)
                    checkedDropDownList.Items.Remove(foundedItems[0]);
            }

            if (!isCheckedItemProcessed)
            {
                isCheckedItemProcessed = true;
                if(e.ChangesType == ComboCheckBoxChangesType.Checked)
                {
                    ListViewItem[] foundedItems = checkedDropDownList.Items.Find(e.Item.ShortText, false);
                    if (foundedItems.Length > 0)
                        foundedItems[0].Checked = e.Item.Checked;
                }
                
                isCheckedItemProcessed = false;
            }

            updateControlText();
        }

        private void checkedDropDownList_LostFocus(object sender, EventArgs e)
        { 
            if (!isIgnoreLostFocusEvent && !this.Focused)
                dropDownedListHide();
        }

        private void checkedDropDownList_KeyPress(object sender, KeyPressEventArgs e)
        { 
            if (e.KeyChar == 27)
                checkedDropDownList.Hide();
        }

        private void checkedDropDownList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!isCheckedItemProcessed)
            {
                isCheckedItemProcessed = true;
                ((ComboCheckBoxItem)(e.Item.Tag)).Checked = e.Item.Checked;
                
                isCheckedItemProcessed = false;
            }

            //e.Item.Selected = true;
        }
        
        private void ComboCheckBox_Click(object sender, EventArgs e)
        {
            
        }

        private void ComboCheckBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
                checkedDropDownList.Hide();
        }

        private void ComboCheckBox_Enter(object sender, EventArgs e)
        {
            isIgnoreLostFocusEvent = true;
            if(!checkedDropDownList.Visible)
            {
                dropedDownListShow();
                isShownOnEnter = true;
            }
            isIgnoreLostFocusEvent = false;
        }

        private void ComboCheckBox_Move(object sender, EventArgs e)
        {
            setPreferedSize();
        }
        #endregion

        #region Private Methods
        private void updateControlText()
        {
            string itemsString = "";
            foreach (ComboCheckBoxItem item in items)
                if (item.Checked)
                    if (itemsString == "")
                        itemsString += item.ShortText;
                    else
                        itemsString += ", " + item.ShortText;
            this.Text = itemsString;
        }

        private void setPreferedSize()
        {
            if (checkedDropDownList.Parent != null)
            {
                checkedDropDownList.Location = checkedDropDownList.Parent.PointToClient(this.PointToScreen(this.Location));
                checkedDropDownList.Width = checkedDropDownList.LargeImageList != null ?
                    checkedDropDownList.LargeImageList.ImageSize.Width * 3 >= MIN_WIDTH ?
                    checkedDropDownList.LargeImageList.ImageSize.Width * 3 : MIN_WIDTH :
                    this.Width >= MIN_WIDTH ? this.Width : MIN_WIDTH;

                int RightEdgeDistance = (checkedDropDownList.Parent.Width - checkedDropDownList.Right) - 20;
                if (RightEdgeDistance < 0)
                    checkedDropDownList.Location -= 
                        new Size (Math.Min(checkedDropDownList.Left, Math.Abs(RightEdgeDistance)), 0);
                int BottomEdgeDistance = checkedDropDownList.Parent.Height - checkedDropDownList.Bottom - 20;
                if (BottomEdgeDistance < 0)
                    checkedDropDownList.Location -= 
                        new Size (0, Math.Min(checkedDropDownList.Top, Math.Abs(BottomEdgeDistance)));
            }
        }

        private void dropedDownListShow()
        {
            if (checkedDropDownList.Parent == null)
                checkedDropDownList.Parent = this.FindForm();
            
            setPreferedSize();

            checkedDropDownList.Show();
            checkedDropDownList.BringToFront();
        }

        private void dropDownedListHide()
        {
            checkedDropDownList.Hide();
        }
        #endregion

        public event ComboCheckBoxItemsChangedEventHandler ItemChanged;

        private const int MIN_WIDTH = 300;
        private const int MIN_HEIGHT = 300;

        private ListView checkedDropDownList;
        private ComboCheckBoxItemsCollection items;
        private bool isCheckedItemProcessed;
        private bool isShownOnEnter;
        private bool isIgnoreLostFocusEvent;

        private string selectedTextOnMouseDown;
        private bool   isTextWasSelected;
    }

    public enum ComboCheckBoxChangesType
    {
        Add,
        Delete,
        Checked
    }

    public delegate void ComboCheckBoxItemsChangedEventHandler(object sender, ComboCheckBoxItemsChangedEventArg e);

    public class ComboCheckBoxItemsChangedEventArg: EventArgs
    {
        public ComboCheckBoxItemsChangedEventArg(ComboCheckBoxItem item, ComboCheckBoxChangesType changesType)
        {
            this.changesType = changesType;
            this.item = item;
        }

        public ComboCheckBoxItem Item
        {
          get { return item; }
          set { item = value; }
        }

        public ComboCheckBoxChangesType ChangesType
        {
          get { return changesType; }
          set { changesType = value; }
        }

        private ComboCheckBoxItem item;
        private ComboCheckBoxChangesType changesType;
    }

    public class ComboCheckBoxItemsCollection : ICollection<ComboCheckBoxItem>, IList<ComboCheckBoxItem>, IEnumerable<ComboCheckBoxItem>
    {
        #region Constructors
        public ComboCheckBoxItemsCollection()
        {
            items = new List<ComboCheckBoxItem>();
        }
        public ComboCheckBoxItemsCollection(ComboCheckBox comboCheckBox, IEnumerable<ComboCheckBoxItem> collection) 
        {
            items = new List<ComboCheckBoxItem>(collection);
            ownedComboCheckBox = comboCheckBox;
        }
        public ComboCheckBoxItemsCollection(ComboCheckBox comboCheckBox) 
        {
            ownedComboCheckBox = comboCheckBox;
            items = new List<ComboCheckBoxItem>();
        }
        #endregion

        #region Public Methods
        public void Add(ComboCheckBoxItem item)
        {
            items.Add(item);
            if (ownedComboCheckBox != null)
                item.OwnerComboCheckBox = ownedComboCheckBox;
            if (ownedComboCheckBox != null)
                    ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(item, ComboCheckBoxChangesType.Add));
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(ComboCheckBoxItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(ComboCheckBoxItem[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Remove(ComboCheckBoxItem item)
        {
            if (ownedComboCheckBox != null)
                ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(item, ComboCheckBoxChangesType.Delete));
            return items.Remove(item);
        }

        public IEnumerator<ComboCheckBoxItem> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public int IndexOf(ComboCheckBoxItem item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, ComboCheckBoxItem item)
        {
            items.Insert(index, item);
            if (ownedComboCheckBox != null)
                ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(item, ComboCheckBoxChangesType.Add));
        }

        public void RemoveAt(int index)
        {
            if (ownedComboCheckBox != null)
                ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(items[index], ComboCheckBoxChangesType.Delete));
            items.RemoveAt(index);
        }

        public void Add(string text) 
        {
            ComboCheckBoxItem newItem = new ComboCheckBoxItem(text);
            if (newItem.OwnerComboCheckBox != null)
                newItem.OwnerComboCheckBox = ownedComboCheckBox;
            this.Add(newItem);
        }
        
        public void Add(string text, bool isChecked) 
        {
            ComboCheckBoxItem newItem = new ComboCheckBoxItem(text, isChecked);
            if (newItem.OwnerComboCheckBox != null)
                newItem.OwnerComboCheckBox = ownedComboCheckBox;
            this.Add(newItem);
        }

        public void Add(string text, int imageIndex) 
        {
            ComboCheckBoxItem newItem = new ComboCheckBoxItem(text, imageIndex);
            if (newItem.OwnerComboCheckBox != null)
                newItem.OwnerComboCheckBox = ownedComboCheckBox;
            this.Add(newItem);
        }

        public void Add(string text, bool isChecked, int imageIndex) 
        {
            ComboCheckBoxItem newItem = new ComboCheckBoxItem(text, isChecked, imageIndex);
            if (newItem.OwnerComboCheckBox != null)
                newItem.OwnerComboCheckBox = ownedComboCheckBox;
            this.Add(newItem);
        }

        public void Add(string shortText, string fullText)
        {
            ComboCheckBoxItem newItem = new ComboCheckBoxItem(shortText, fullText);
            if (newItem.OwnerComboCheckBox != null)
                newItem.OwnerComboCheckBox = ownedComboCheckBox;
            this.Add(newItem);
        }
        #endregion

        #region Public Properties
        public int Count
        { 
            get { return items.Count; } 
        }

        public bool IsReadOnly
        { 
            get { return false; } 
        }

        public ComboCheckBoxItem this[int index] 
        { 
            get { return items[index]; }
            set { 
                if (ownedComboCheckBox != null)
                    ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(items[index], ComboCheckBoxChangesType.Delete));
                
                items[index] = value;

                if (ownedComboCheckBox != null)
                    ownedComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(value, ComboCheckBoxChangesType.Add));
            }
        }
        #endregion

        private ComboCheckBox ownedComboCheckBox;
        private List<ComboCheckBoxItem> items;
    }

    public class ComboCheckBoxItem
    {
        #region Constructors
        public ComboCheckBoxItem()
        {
            initFields("", false, -1, "");
        }

        public ComboCheckBoxItem(string text)
        {
            initFields(text, false, -1, "");
        }

        public ComboCheckBoxItem(string text, bool isChecked)
        {
            initFields(text, isChecked, -1, "");
        }

        public ComboCheckBoxItem(string text, int imageIndex)
        {
            initFields(text, false, imageIndex, "");
        }

        public ComboCheckBoxItem(string text, bool isChecked, int imageIndex)
        {
            initFields(text, false, imageIndex, "");
        }

        public ComboCheckBoxItem(string shortText, bool isChecked, int imageIndex, string fullText)
        {
            initFields(shortText, false, imageIndex, fullText);
        }

        public ComboCheckBoxItem(string shortText, string fullText)
        {
            initFields(shortText, false, -1, fullText);
        }
        #endregion

        #region Public Properties
        public string ShortText
        {
            get { return shortText; }
            set { shortText = value; }
        }

        public string FullText
        {
            get { return fullText; }
            set { fullText = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        } 

        public bool Checked
        {
            get { return isChecked; }
            set {
                isChecked = value;
                if (ownerComboCheckBox != null) 
                    ownerComboCheckBox.OnChangedItems(new ComboCheckBoxItemsChangedEventArg(this, ComboCheckBoxChangesType.Checked));
            }
        }

        public int ImageIndex
        {
            get { return imageIndex; }
            set { imageIndex = value; }
        }

        public ComboCheckBox OwnerComboCheckBox
        {
            get { return ownerComboCheckBox; }
            set { ownerComboCheckBox = value; }
        }
        #endregion

        #region Private Methods
        private void initFields(string shortText, bool isChecked, int imageIndex, string fullText)
        {
            this.shortText = shortText;
            this.isChecked = isChecked;
            this.imageIndex = imageIndex;
            this.fullText = fullText;
        }
        #endregion

        #region Private Filds
        ComboCheckBox ownerComboCheckBox;        
        private string shortText;
        private string fullText;
        private bool isChecked;
        private int imageIndex;
        private string name;
        private object tag;
        #endregion
    }
}
