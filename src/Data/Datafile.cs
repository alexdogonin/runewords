using System.Linq;
using System.Collections.Generic;
using Zyel.Data.Entities;

namespace Zyel.Data
{
  /// <summary>
  /// Describes structure of the datafile
  /// </summary>
  public class FileData
  {
    public FileData()
    {
      Runes = new Dictionary<string, Zyel.Data.Entities.Rune>();
      ItemClasses = new Dictionary<string, Zyel.Data.Entities.ItemClass>();
      PersonClasses = new Dictionary<string, Zyel.Data.Entities.PersonClass>();
      Runewords = new Dictionary<string, Zyel.Data.Entities.Runeword>();
    }
    public Dictionary<string, Zyel.Data.Entities.Rune> Runes { get; set; }
    public Dictionary<string, Zyel.Data.Entities.ItemClass> ItemClasses { get; set; }
    public Dictionary<string, Zyel.Data.Entities.PersonClass> PersonClasses { get; set; }
    public Dictionary<string, Zyel.Data.Entities.Runeword> Runewords { get; set; }
  }
}