using YamlDotNet.Serialization;

namespace Zyel.Data.Entities
{
  public class PersonClass
  {
    [YamlMember(DefaultValuesHandling = DefaultValuesHandling.Preserve)]
    public string Name { get; set; }
    public string Description { get; set; }
  }
}