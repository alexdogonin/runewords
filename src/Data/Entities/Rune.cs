using YamlDotNet.Serialization;

namespace Zyel.Data.Entities
{
  public class Rune
  {
    [YamlMember(DefaultValuesHandling = DefaultValuesHandling.Preserve)]
    public int Level { get; set; }
    public string Description { get; set; }
  }
}