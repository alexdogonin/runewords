using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace Zyel.Data.Entities
{
  public class ItemClass
  {
    [YamlMember(DefaultValuesHandling = DefaultValuesHandling.Preserve)]
    public string Name { get; set; }
    public Dictionary<string, ItemClass> Classes { get; set; }
    public string Description { get; set; }
  }
}