using System.Collections.Generic;
using System.Linq;
using YamlDotNet.Serialization;

namespace Zyel.Data.Entities
{
  public class Runeword
  {
    [YamlMember(DefaultValuesHandling = DefaultValuesHandling.Preserve)]
    public List<string> Runes { get; set; }
    public List<string> ItemClasses { get; set; }
    public List<string> PersonClasses { get; set; }
    // TODO this field can be determined from runes
    public int Level { get; set; }
    public string Description { get; set; }
  }
}