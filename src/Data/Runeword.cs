using System.Collections.Generic;

namespace Zyel.Data
{
  public class Runeword
  {
    public string Name { get; init; }
    public int Level { get; init; }
    public ItemClass[] ItemClasses { get; init; }
    public PersonClass[] PersonClasses { get; init; }
    public Rune[] Runes { get; init; }
  }
}