using System;
using System.Linq;

namespace Zyel.Data
{
  public class RunewordsFilter
  {
    ///<summary>
    ///  Item class name
    ///</summary>
    public string Name { get; set; }
    public Range? Level { get; set; }
    public string[] ItemClasses { get; set; }
    public string[] PersonClasses { get; set; }
    public string[] Runes { get; set; }
    public int[] SocketsCount { get; set; }

    public bool Check(Runeword runeword)
    {
      if (Name?.Length > 0)
        if (!runeword.Name.ToLower().Contains(Name.ToLower()))
          return false;

      if (Level is not null)
        if (!Level.Value.Includes(runeword.Level))
          return false;

      if (ItemClasses?.Length > 0)
        if (ItemClasses.Intersect(runeword.ItemClasses.Select(i => i.ShortName)).First() is null)
          return false;

      if (PersonClasses?.Length > 0)
        if (PersonClasses.Intersect(runeword.PersonClasses.Select(p => p.ShortName)).First() is null)
          return false;

      if (Runes?.Length > 0)
        if (Runes.Intersect(runeword.Runes.Select(r => r.Name)).Count() == Runes.Length)
          return false;

      if (SocketsCount?.Length > 0)
        if (!SocketsCount.Contains(runeword.Runes.Length))
          return false;

      return true;
    }
  }
}