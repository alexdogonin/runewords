namespace Zyel.Data
{
  public class PersonClass
  {
    public string ShortName { get; init; }
    public string Name { get; init; }
  }
}