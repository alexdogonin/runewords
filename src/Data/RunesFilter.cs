namespace Zyel.Data
{
  public class RunesFilter
  {
    public string Name { get; set; }
    public Range Level { get; set; }

    public bool Check(Rune rune) => rune.Name.ToLower().Contains(Name.ToLower()) && Level.Includes(rune.Level);
  }
}