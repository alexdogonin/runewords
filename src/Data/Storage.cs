using System.Collections.Generic;
using System.Linq;

namespace Zyel.Data
{
  /// <summary>
  /// Implements work with data
  /// </summary>
  public class Storage
  {
    public Storage(FileData data)
    {
      this.data = data;
    }

    public IEnumerable<Runeword> GetRunewords(RunewordsFilter filter = null)
    {
      var runewords = data.Runewords
      .Select(r => new Runeword
      {
        Name = r.Key,
        Level = r.Value.Level,
        ItemClasses = r.Value.ItemClasses.Select(ic =>
          {
            var itemClassData = data.ItemClasses[ic];

            return new ItemClass { ShortName = ic, Name = itemClassData.Name };
          })
          .ToArray(),
        PersonClasses = r.Value.PersonClasses.Select(pc =>
          {
            var personClassData = data.PersonClasses[pc];

            return new PersonClass { ShortName = pc, Name = personClassData.Name };
          })
          .ToArray(),
        Runes = r.Value.Runes.Select(r =>
          {
            var runeData = data.Runes[r];

            return new Rune { Name = r, Description = runeData.Description, Level = runeData.Level };
          })
          .ToArray()
      });

      if (filter is not null)
        runewords = runewords.Where(r => filter.Check(r));

      return runewords;
    }

    public IEnumerable<Rune> GetRunes(RunesFilter filter = null)
    {
      var runes = data.Runes
        .Select(r =>
        {
          var runeData = data.Runes[r.Key];

          return new Rune { Name = r.Key, Description = r.Value.Description, Level = r.Value.Level };
        });

      if (filter is not null)
        runes = runes.Where(r => filter.Check(r));

      return runes;
    }

    public IEnumerable<ItemClass> GetItemClasses()
    {
      return data.ItemClasses
      .Select(ic =>
      {
        var itemClassData = data.ItemClasses[ic.Key];

        return new ItemClass { ShortName = ic.Key, Name = ic.Value.Name };
      });
    }

    public IEnumerable<PersonClass> GetPersonClasses()
    {
      return data.PersonClasses
      .Select(pc =>
      {
        var personClassData = data.PersonClasses[pc.Key];

        return new PersonClass { ShortName = pc.Key, Name = pc.Value.Name };
      });
    }

    /// <remarks>Will be implemented in the future</remarks>
    /// <exception cref="System.NotImplementedException"/>
    public IEnumerable<object> GetReceipts(object filter = null) => throw new System.NotImplementedException();

    FileData data;
  }






}

