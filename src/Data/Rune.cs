using System;
using System.Runtime.CompilerServices;

namespace Zyel.Data
{
  public class Rune
  {
    public string Name { get; init; }
    public int Level { get; init; }
    public string Description { get; init; }
  }
}