namespace Zyel.Data
{
  public struct Range
  {
    public int From { get; set; }
    public int To { get; set; }

    public bool Includes(int value) => From <= value && To >= value;
  }
}