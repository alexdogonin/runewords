using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using YamlDotNet.Serialization.NamingConventions;

using Zyel.Data;
using Zyel.Data.Entities;
using YamlDotNet.Serialization;
using CommandLine;

namespace Zyel
{
  static class Program
  {
    static int Main(string[] args)
    {
      Config config = null;
      CommandLine.Parser.Default.ParseArguments<Config>(args).WithParsed(c => config = c);

      var builder = new YamlDotNet.Serialization.DeserializerBuilder();
      var deserializer = builder
          .WithNamingConvention(CamelCaseNamingConvention.Instance)
          .Build();


      try
      {
        var f = new System.IO.StreamReader(config.Filename);

        var datafile = deserializer.Deserialize<Zyel.Data.Datafile>(f);

        var storage = new Zyel.Data.Storage(
          runewords: datafile.Runewords.Select(
            rw => new Zyel.Data.Runeword
            {
              Name = rw.Key,
              Level = rw.Value.Level,
              ItemClasses = rw.Value.ItemClasses.Select(
                ic => new Data.ItemClass
                {
                  ShortName = ic,
                  Name = datafile.ItemClasses[ic].Name,
                }
              ).ToList(),
              PersonClasses = rw.Value.PersonClasses.Select(
                pc => new Data.PersonClass
                {
                  ShortName = pc,
                  Name = datafile.PersonClasses[pc].Name,
                }
              ).ToList(),
              Runes = rw.Value.Runes.Select(
                r => new Data.Rune
                {
                  Name = r,
                  Level = datafile.Runes[r].Level,
                }
              ).ToList()
            }
          )
        );

        var runewords = storage.GetRunewords(new RunewordsFilter
        {
          Level = new Data.Range { From = config.Level.ElementAt(0), To = config.Level.ElementAt(1) },
        });

        var serializer = new SerializerBuilder()
        .ConfigureDefaultValuesHandling(DefaultValuesHandling.OmitDefaults)
        .Build();

        serializer.Serialize(Console.Out, runewords);
      }
      catch (ApplicationException e)
      {
        Console.WriteLine($"exec error, ${e.ToString()}");
        return 1;
      }


      return 0;
    }
  }

  class Config
  {
    [Option('f', "filename", Default = "./datafile.yaml", Required = true, HelpText = "datafile path")]
    public string Filename { get; private set; }
    [Option('l', "level", Default = new int[] { 0, 255 }, Min = 2, Max = 2, HelpText = "runeword level, format: <min>,<max>", Separator = ',')]
    public IEnumerable<int> Level { get; private set; }
    [Option('r', "runes", HelpText = "coma-separated rune names", Separator = ',')]
    public IEnumerable<string> Runes { get; private set; }
    [Option('p', "person-classes", HelpText = "coma-separated person classes", Separator = ',')]
    public IEnumerable<string> PersonClasses { get; private set; }
    [Option('i', "item-classes", HelpText = "coma-separated item classes", Separator = ',')]
    public IEnumerable<string> ItemClasses { get; private set; }
    [Option('s', "sockets", HelpText = "sockets count")]
    public int? Sockets { get; private set; }
  }


}