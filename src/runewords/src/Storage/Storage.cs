using System.Linq;
using System.Collections.Generic;
using Diablo2ZyElRunewords.Storage.Entities;

namespace Diablo2ZyElRunewords.Storage
{
	public class Storage
	{
		public Dictionary<string, Rune> Runes { get; set; }
		public Dictionary<string, ItemClass> ItemClasses { get; set; }
		public Dictionary<string, PersonClass> PersonClasses { get; set; }
		public List<Runeword> Runewords { get; set; }

		public override string ToString() => $@"
Runes: 
	{Runes?.Aggregate("", (str, pair) => $"{str}{pair.Key} {pair.Value}\n")}

PersonClasses: 
	{PersonClasses?.Aggregate("", (str, pair) => $"{str}{pair.Key} {pair.Value}\n")}

ItemClasses: 
	{ItemClasses?.Aggregate("", (str, pair) => $"{str}{pair.Key} {pair.Value}\n")}

Runewords: 
	{Runewords?.Aggregate("", (str, item) => $"{str}{item}\n")}
";

	}
}