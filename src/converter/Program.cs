﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using Zyel.Data;
using Zyel.Data.Entities;

namespace Converter
{
  class Program
  {
    static string usage = $@"usage:
converter <input_file>
";
    static int Main(string[] args)
    {
      if (args.Length != 1)
      {
        Console.Write(usage);
        return 1;
      }

      var lines = File.ReadLines(args[0]);

      var storage = new Datafile();


      blocks? currentBlock = null;
      foreach (var line in lines)
      {
        var match = System.Text.RegularExpressions.Regex.Match(line, @"(?<=\[)\w+(?=\])");

        if (match.Value != "")
        {
          switch (match.Value.ToLower())
          {
            case "itemclasses":
              currentBlock = blocks.itemClass;
              break;
            case "personclasses":
              currentBlock = blocks.personClass;
              break;
            case "runes":
              currentBlock = blocks.rune;
              break;
            case "runewords":
              currentBlock = blocks.runeword;
              break;
          }

          continue;
        }

        if (currentBlock == null)
        {
          continue;
        }

        string trimmedLine = line.Trim();
        var commentInd = trimmedLine.IndexOf('#');

        if (commentInd != -1) trimmedLine = trimmedLine.Substring(0, commentInd - 1);

        if (trimmedLine.Length == 0) continue;

        var lineParts = trimmedLine.Split('\t');

        switch (currentBlock)
        {
          case blocks.itemClass:
            if (lineParts.Length < 2) throw new ApplicationException($"item class line wrong format '{trimmedLine}'");

            storage.ItemClasses[lineParts[0]] = new Zyel.Data.Entities.ItemClass
            {
              Name = lineParts[1],
              Description = lineParts.Length >= 4 ? lineParts[3] : null
            };

            break;
          case blocks.personClass:
            if (lineParts.Length < 2) throw new ApplicationException($"item class line wrong format '{trimmedLine}'");

            storage.PersonClasses[lineParts[0]] = new Zyel.Data.Entities.PersonClass
            {
              Name = lineParts[1],
              Description = lineParts.Length >= 3 ? lineParts[2] : null
            };

            break;
          case blocks.rune:
            if (lineParts.Length < 2) throw new ApplicationException($"item class line wrong format '{trimmedLine}'");
            storage.Runes[lineParts[0]] = new Zyel.Data.Entities.Rune
            {
              Level = int.Parse(lineParts[1]),
              Description = lineParts.Length >= 3 ? lineParts[2] : null
            };

            break;
          case blocks.runeword:
            if (lineParts.Length < 5) throw new ApplicationException($"item class line wrong format '{trimmedLine}'");
            storage.Runewords[lineParts[0]] = new Zyel.Data.Entities.Runeword
            {
              ItemClasses = lineParts[1].Split(',').ToList(),
              Level = int.Parse(lineParts[2]),
              PersonClasses = new List<string> { lineParts[3] },
              Runes = lineParts[4].Split('-').ToList(),
              Description = lineParts.Length >= 6 ? lineParts[5] : null
            };
            break;
        }
      }

      var serializer = new YamlDotNet.Serialization.SerializerBuilder()
          .ConfigureDefaultValuesHandling(DefaultValuesHandling.OmitDefaults)
          .WithNamingConvention(CamelCaseNamingConvention.Instance)
          .Build();

      serializer.Serialize(Console.Out, storage);

      //   var m = new Dictionary<string, object>
      //   {
      //     ["el"] = new { level = 5 },
      //     ["zy"] = new { level = 5, description = "ты её никогда не используешь!" },
      //   };

      //   new YamlDotNet.Serialization.SerializerBuilder()
      //               .ConfigureDefaultValuesHandling(DefaultValuesHandling.OmitDefaults)
      //               .WithNamingConvention(CamelCaseNamingConvention.Instance)
      //               .Build()
      //               .Serialize(Console.Out, m);

      //   var builder = new YamlDotNet.Serialization.DeserializerBuilder();
      //   var deserializer = builder
      //       .WithNamingConvention(CamelCaseNamingConvention.Instance)
      //       .Build();

      //   var d = new System.IO.StreamReader(args[0]);

      //   var storage = deserializer.Deserialize<Datafile>(d);

      //   void printItemClass(string prefix, string shortName, ItemClass item)
      //   {
      //     Console.WriteLine($"{prefix}{shortName}\t{item.Name}\t{item.Description}");

      //     if (item.Classes == null) return;

      //     foreach (var inner in item.Classes)
      //     {
      //       printItemClass(prefix + "  ", inner.Key, inner.Value);
      //     }
      //   };

      //   foreach (var item in storage.ItemClasses)
      //   {
      //     printItemClass("", item.Key, item.Value);
      //   }

      //   Console.WriteLine();

      //   foreach (var rune in storage.Runes)
      //     Console.WriteLine($"{rune.Key}\t{rune.Value.Level}\t{rune.Value.Description}");

      //   Console.WriteLine();

      //   foreach (var pClass in storage.PersonClasses)
      //     Console.WriteLine($"{pClass.Key}\t{pClass.Value.Name}\t{pClass.Value.Description}");

      //   Console.WriteLine();

      //   foreach (var runeword in storage.Runewords)
      //   {
      //     var itemClasses = runeword.Value.ItemClasses != null ? string.Join(',', runeword.Value.ItemClasses) : "";
      //     var personClasses = runeword.Value.PersonClasses != null ? string.Join('-', runeword.Value.PersonClasses) : "";
      //     var runes = runeword.Value.Runes != null ? string.Join('-', runeword.Value.Runes) : "";
      //     Console.WriteLine($"{runeword.Key}\t{itemClasses}\t{runeword.Value.Level}\t{personClasses}\t{runes}");
      //   }

      return 0;
    }
  }

  enum blocks
  {
    rune,
    itemClass,
    personClass,
    runeword
  };
}
