using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using YamlDotNet;
using YamlDotNet.Serialization.NamingConventions;
using Zyel.Data;
using MatBlazor;

namespace Site
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      var builder = WebAssemblyHostBuilder.CreateDefault(args);
      builder.RootComponents.Add<App>("app");
      builder.Services.AddMatToaster(config =>
            {
              //example MatToaster customizations
              config.PreventDuplicates = false;
              config.NewestOnTop = true;
              config.ShowCloseButton = true;
            });

      var httpClient = new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) };
      builder.Services.AddScoped(sp => httpClient);

      var data = await httpClient.GetJsonAsync<FileData>("sample-data/data.json");
      var storage = new Storage(data);
      builder.Services.AddScoped<Storage>(sp => storage);

      var runApp = builder.Build().RunAsync();

      await runApp;
    }
  }
}
